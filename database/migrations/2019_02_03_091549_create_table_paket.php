<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tbl_paket', function (Blueprint $table) {
           $table->increments('id_paket');
           $table->integer('paket');
           $table->string('deskripsi_paket',255);
           $table->integer('waktu_pengerjaan')->length(20);
           $table->timestamp('created_at');
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('tbl_paket');
     }
}
