<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSoal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('tbl_soal', function (Blueprint $table) {
          $table->increments('id_soal');
          $table->enum('tipe_soal',['reading','listening']);
          $table->text('soal_tulisan');
          $table->text('soal_gambar');
          $table->text('soal_suara');
          $table->string('jawaban_a',255);
          $table->string('jawaban_b',255);
          $table->string('jawaban_c',255);
          $table->string('jawaban_d',255);
          $table->string('jawaban_asli',255);
          $table->text('keterangan');
          $table->integer('id_paket')->length(10);
          $table->timestamp('waktu_pembuatan');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::dropIfExists('tbl_soal');
    }
}
