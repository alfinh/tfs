@extends('templates.frontend')
@section('content')
<div class="container" >
  <div class="row">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h1>Cara Daftar</h1>
      </div>
      <div class="panel-body">
        <h4><b>Gimana cara daftar tes TOEIC?</b></h4><br>
        <p>Pertama-tama, masuk ke website <i>itc-indonesia.com</i>. Kalian bikin akun disana. Pilih jadwal dan lokasi tes yang sesuai kebutuhan
          teman-teman. Jangan lupa perhatikan jadwal tes dan batas akhir pendaftaran ya. Setahu saya, tes TOEIC ini hanya diadakan sekali
          dalam sebulan. Batas pendaftaran ditutup H-3 jadwal tes. Untuk melihat jadwal, kalian bisa masuk ke website <i>itc-indonesia.com</i>.
          Selanjutnya, pilih cara pembayaran. Oh iya, biaya tesnya Rp.700.000. Lumayan agak mahal sih dibanding TOEFL ITP, tapi worth it.</p><br>

          <h4><b>Apa yang harus dibawa ketika tes?</b></h4><br>
          <p>KTP dan Printout konfirmasi pembayaran dan pendaftaran. Cuman itu aja, alat tulis disediakan disana.</p><br>

          <h4><b>Sebelum tes dimulai, ngapain?</b></h4><br>
          <p>Jadwal tes seharusnya dari jam 09.00 sampai jam 11.30. Tapi, jam 09.00 para peserta tes baru dipanggil
            satu persatu oleh satu orang supervisor ruangan. Peserta tes diminta menunjukkan KTP dan Printout konfirmasi pendaftaran,
            kemudian dicocokkan dan difoto. Setelah itu, sebelum masuk ruangan tes, kita akan di-scan pake alat scan metal.
            Hampir menghabiskan waktu setengah jam lebih untuk menunggu semua peserta sudah terkonfirmasi dan tentunya, di-scan.</p>
      </div>
    </div>
  </div>
</div>
@endsection
