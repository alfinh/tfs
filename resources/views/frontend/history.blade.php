@extends('templates/user')

@section('content')
<div class="container"><br><br><br><br>
  <div class="row" style="position:relative; top:20px;">
    <div class="panel">
      <div class="panel-heading">
        <h2>History</h2>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table">
            <tr>
              <td><b>Kode Test</b></td>
              <td><b>Keterangan</b></td>
              <td><b>Nilai</b></td>
              <td><b>Waktu pengerjaan</b></td>
              <td><b>Status Pengerjaan</b></td>
            </tr>
            @foreach($history as $data)
            <tr>
              <td>{{$data->id_tester}}</td>
              <td>Anda telah mengerjakan paket {{$data->id_paket}} </td>
              <td>{{$data->nilai}}</td>
              <td>60 Menit</td>

              @if($data->nilai == 0)
              <td><b style="color:crimson;"><i>Belum Selesai</i></b></td>
              @else
              <td><b style="color:green;"><i>Telah Selesai</i></b></td>
              @endif
              
            </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
</div><br><br><br><br><br><br><br>
@endsection
