<html>

<head>
    <meta charset="utf-8">
    <meta name="author" content="John Doe">
    <meta name="description" content="">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Title -->
    <title>TOEIC</title>
    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="{{asset('assets')}}/images/apple-touch-icon.png">
    <link rel="shortcut icon" type="{{asset('assets')}}/image/ico" href="{{asset('assets')}}/images/favicon.ico" />
    <!-- Plugin-CSS -->

    <link rel="stylesheet" href="{{asset('assets')}}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/themify-icons.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/magnific-popup.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/animate.css">
    <!-- Main-Stylesheets -->
    	<link rel="stylesheet" href="{{asset('assets')}}/css/main.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/normalize.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/style2.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/responsive.css">
    <script src="{{asset('assets')}}/js/vendor/modernizr-2.8.3.min.js"></script>

    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body data-spy="scroll" data-target="#primary-menu" style="background-color:#eee;">
<br><br>
<div class="container">
    <div class="row">
      <div class="col-sm-6 col-sm-offset-1">
        <div class="panel-primary" style="border:2px solid #4169E1;">
          <div class="panel-heading" style=" border:1px solid skyblue;">
            <center><h2 style="color:white;">NILAI TEST</h2></center>
          </div>
          <div class="panel-body" style="background-image: url('../assets/images/angle-bg.png');">
            <br>
            <center><span style="font-size:145px; color:#4169E1; border:3px solid #4169E1; padding:15px;">{{$data->nilai}}</span></center>
            <br>
            <div class="col-sm-8 col-sm-offset-2 " style="position: relative; left:20px;">
              <p style="padding:5px; color:#4169E1; font-size:20px;"><u>Kode Test :  {{$data->id_tester}} </u></p>
              <p style="padding:5px; color:#4169E1; font-size:20px;"><u>Nilai   : {{$data->nilai}}</u></p><br>
              <p style="padding:5px; color:skyblue; font-size:20px;"><u>Paket : {{$data->id_paket}}</u></p>
              <p style="padding:5px; color:skyblue; font-size:20px;"><u>Waktu Pengerjaan : 60 Menit</u></p><br>

            </div>
          </div>
      </div>
   </div>
   <div class="col-sm-4">
     <form action="{{URL('/validasikomentar')}}" method="post">
       {{csrf_field()}}
      <label><h3 style="margin:5px;"><u>Komentar</u></h3></label>
      <textarea class="form-control" name="komentar" width="7"></textarea>
      <button type="submit" class="btn btn-sm btn-fill btn-info " style="position: relative;bottom:10px;">Kirim</button>
    </form>
    <b><i>*Kirimkan komentar kritik dan saranmu<br>
          *Anda wajib mengisinya</i></b>
    <br><br><br><br><br><br><br>
    <center>
    @if($data->nilai <= 30)
    <h2 style="border:2px solid #4169E1;padding:10px;  color:#4169E1;">Beginner</h2><span style="color:#4169E1;">_________________________________________</span><br><br>
    <p style="font-size:20px; color:#4169E1;">"You have to learn a lot in English to get a more satisfying value"</p>
    @elseif($data->nilai >=30 || $data->nilai <= 70)
    <h2 style="border:2px solid #4169E1; padding:10px; color:#4169E1;">Intermediate</h2><span style="color:#4169E1;">_________________________________________</span><br><br>
    <p style="font-size:20px; color:#4169E1;"><i>"At least you already understand and can start working on it but you have to increase it even more"</i></p>
    @else
    <h2 style="border:2px solid #4169E1; color:#4169E1;">Advanced</h2><span style="color:#4169E1;">_________________________________________</span><br><br>
      <p style="font-size:20px; color:#4169E1;"><i>"
        You have entered a high level, you are ready for the real toeic test"</i></p>
    @endif
    <br>
    <span style="color:#4169E1;">_________________________________________</span
    </center>
   </div>
  </div>
</div>`<br><br>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>

</body>
</html>
