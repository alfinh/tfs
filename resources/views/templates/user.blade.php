<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="author" content="John Doe">
    <meta name="description" content="">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Title -->
    <title>TFS</title>
    <style>
    d: #fff
    }
    .panel-heading a {
        background: #f7f7f7;
        border-radius: 5px;
    }

    .nav.nav-tabs li a,
    .nav.nav-tabs li.active > a:hover,
    .nav.nav-tabs li.active > a:active,
    .nav.nav-tabs li.active > a:focus {
        border-bottom-width: 0px;
        outline: none;
    }

    .tab-pane {
        background: #fff;
        padding: 10px;
        border: 1px solid #eee;
        margin-top: -1px;
    }



    /* used for sidebar tab/collapse*/
    @media (max-width: 991px) {
      .visible-tabs {
        display: none;
      }
    }

    @media (min-width: 992px) {
      .visible-tabs {
        display: block !important;
      }
    }

    @media (min-width: 992px) {
      .hidden-tabs {
        display: none !important;
      }
    }
    </style>
    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="{{asset('assets')}}/images/apple-touch-icon.png">
    <link rel="shortcut icon" type="{{asset('assets')}}/image/ico" href="{{asset('assets')}}/images/favicon.ico" />
    <!-- Plugin-CSS -->
    <link rel="stylesheet" href="{{asset('assets')}}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/themify-icons.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/magnific-popup.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/animate.css">
    <!-- Main-Stylesheets -->
    <link rel="stylesheet" href="{{asset('assets')}}/css/normalize.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/style.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/style2.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/responsive.css">
    <script src="{{asset('assets')}}/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body data-spy="scroll" data-target="#primary-menu" style="background:#F9F9F9;">

      <div class="preloader">
         <div class="sk-folding-cube">
             <div class="sk-cube1 sk-cube"></div>
             <div class="sk-cube2 sk-cube"></div>
             <div class="sk-cube4 sk-cube"></div>
             <div class="sk-cube3 sk-cube"></div>
         </div>
     </div>
     <div class="top_header" id="home" >
       <!-- Fixed navbar -->
       <nav class="navbar navbar-default navbar-fixed-top" >
         <div class="nav_top_fx_w3ls_agileinfo">
           <div class="navbar-header">
             <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                 aria-controls="navbar">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
             </button>
             <div class="logo-w3layouts-agileits" style="margin-top:5px;">
               <h1> <a class="navbar-brand" href="#">TOEIC <span class="desc">Test English</span></a></h1>
             </div>
           </div>
           <div id="navbar" class="navbar-collapse collapse" style="opacity:0.8;">
             <div class="nav_right_top">
               <ul class="nav navbar-nav navbar-right" style="position:relative; bottom:3px;">
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    {{Auth::user()->nama}}  <img src="{{asset('assets')}}/images/uf1.png" width="30px" style="border-radius:50%;"></i> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="/profile"><i class="glyphicon glyphicon-user"></i> Profile</a></li>
                      <li><a href="#" data-toggle="modal" data-target="#ubahpassword"><i class="glyphicon glyphicon-lock"></i> Ubah Password</a></li>
                      <li><a href="{{ url('user/logout') }}"
                         onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();"><i class="glyphicon glyphicon-log-out"></i> Keluar</a></li>
                    </ul>
                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                    </form>
                   </li>
                </ul>
               <ul class="nav navbar-nav">
                 <li class="active"><a href="{{url('/dashboarduser')}}">Dashboard</a></li>
                 <li><a href="/history">History</a></li>
               </ul>
             </div>
           </div>
           <!--/.nav-collapse -->
         </div>
       </nav>
     </div>
     @yield('content')
    <footer class="footer-area relative sky-bg" id="contact-page">
        <div class="absolute footer-bg"></div>
          <div class="footer-bottom">
              <div class="container">
                  <div class="row">
                      <div class="col-xs-12 text-center">
                          <p>&copy;<span style="color:white;">Copyright 2018 All right resurved. by Alfi & Aziz</span></p>
                      </div>
                  </div>
              </div>
          </div>
    </footer>
    <div class="modal fade" tabindex="-1" id="ubahpassword" role="dialog" data-backdrop="true">
       <div class="modal-dialog" role="document">
         <div class="modal-content">
      <!-- Modal Header -->
           <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true"> &times;</span>
             </button>
                 <h3 class="modal-title"><b>Ubah Password</b></h3>
           </div>

               <form action="/ubahpassworduser" method="POST" autocomplete="off">
                {{ csrf_field() }}
                <div class="modal-body">
                  <div class="form-group" style="position: relative; top:10px;">
                    <label for="name">Password Baru</label>
                    <input type="password"  name="passwordbaru" class="form-control" placeholder="Masukkan Password Baru" required>
                  </div>

                  <div class="form-group" style="position: relative; top:10px;">
                    <label for="name">Confirm Password</label>
                    <input type="password"  name="passwordconfirm" class="form-control" placeholder="Ketik ulang password baru" required>
                  </div>
               </div>

              <div class="modal-footer">
                 <button type="reset" class="btn btn-raised btn-default "><i class="glyphicon glyphicon-refresh"> </i> Reset</button>
                 <button type="submit" class="btn btn-primary btn-save pull-right"><i class="glyphicon glyphicon-floppy-disk"></i> Submit</button>
              </div>
           </form>
       </div>
    </div>
  </div>

<!--Vendor-JS-->
<script src="{{asset('assets')}}/js/vendor/jquery-1.12.4.min.js"></script>
<script src="{{asset('assets')}}/js/vendor/bootstrap.min.js"></script>
<!--Plugin-JS-->
<script>
$(document).ready(function() {

  // DEPENDENCY: https://github.com/flatlogic/bootstrap-tabcollapse


  // if the tabs are in a narrow column in a larger viewport
  $('.sidebar-tabs').tabCollapse({
      tabsClass: 'visible-tabs',
      accordionClass: 'hidden-tabs'
  });

  // if the tabs are in wide columns on larger viewports
  $('.content-tabs').tabCollapse();

  // initialize tab function
  $('.nav-tabs a').click(function(e) {
      e.preventDefault();
      $(this).tab('show');
  });

  // slide to top of panel-group accordion
  $('.panel-group').on('shown.bs.collapse', function() {
      var panel = $(this).find('.in');
      $('html, body').animate({
          scrollTop: panel.offset().top + (-60)
      }, 500);
  });

});
</script>
<script src="{{asset('assets')}}/js/owl.carousel.min.js"></script>
<script src="{{asset('assets')}}/js/contact-form.js"></script>
<script src="{{asset('assets')}}/js/jquery.parallax-1.1.3.js"></script>
<script src="{{asset('assets')}}/js/scrollUp.min.js"></script>
<script src="{{asset('assets')}}/js/magnific-popup.min.js"></script>
<script src="{{asset('assets')}}/js/wow.min.js"></script>
<!--Main-active-JS-->
<script src="{{asset('assets')}}/js/main.js"></script>
</body>

</html>
