<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BackendController extends Controller
{
    public function login()
    {
      return view('login');
    }
    public function dashboard()
    {
      $bulanini =  Carbon::now()->format('M');
      $bulansebelumnya =  Carbon::now()->subMonth(1)->format('M');
      $bulanini2 =  Carbon::now()->format('m');
      $bulansebelumnya2 =  Carbon::now()->subMonth(1)->format('m');

      $soalreading = DB::table('tbl_soal')->where('tipe_soal','=','reading')->count();
      $soallistening = DB::table('tbl_soal')->where('tipe_soal','=','listening')->count();
      $usertester = DB::table('tbl_tester')->count();
      $userregister = DB::table('users')->where('isadmin','=',0)->count();

      $yearnow = Carbon::now()->format('Y');
      $monthnow = Carbon::now()->format('m');

      return view('backend/dashboard',['userregister'=>$userregister,'soalreading'=>$soalreading, 'soallistening'=>$soallistening, 'usertester'=>$usertester,'bulanini'=>$bulanini, 'bulansebelumnya'=>$bulansebelumnya]
                                       );
    }

}
