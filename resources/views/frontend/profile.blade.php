@extends('templates/user')

@section('content')
      <div class="container"><br><br><br><br><br><br>
        <div class="row">
          <div class="col-md-8" style="border:1px solid lightgrey; padding:15px; border-radius:1%;">
          <div class="panel-primary">
              <div class="panel-heading">
                  <h2 class="title">Edit Profile </h2>
                  {{-- part alert --}}
               @if (Session::has('after_save'))
                       <div class="col-md-12">
                           <div class="alert alert-dismissible alert-{{ Session::get('after_save.alert') }}">
                             <i class="pe-7s-{{ Session::get('after_save.icon') }}" style="font-size:30px; position:relative; top:8px;"></i>
                             <button type="button" class="close" data-dismiss="alert">×</button>
                             <strong>{{ Session::get('after_save.title') }}</strong>
                             <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_save.text-1') }}</a> {{ Session::get('after_save.text-2') }}
                           </div>
                       </div>
               @endif
         {{-- end part alert --}}
       </div><br>
          <div class="content">
                <form action="{{URL('/editakun')}}" method="post" enctype="multipart/form-data" autocomplete="off">
                 {{ csrf_field() }}
                   <div class="row">
                     <div class="col-md-6">
                         <div class="form-group">
                             <label>ID User</label>
                             <input type="text" name="id_toko" class="form-control" readonly value="{{Auth::user()->id}}">
                         </div>
                     </div>
                     <div class="col-md-6">
                         <div class="form-group">
                             <label>Nama User</label>
                             <input type="text" name="nm_user" class="form-control" placeholder="nama user" maxlength="30" value="{{Auth::user()->nama}}" required>
                         </div>
                     </div>
                    </div>

                    <div class="row">
                       <div class="col-md-6">
                           <div class="form-group">
                               <label>Username</label>
                               <input type="text" name="username" class="form-control" placeholder="Username" maxlength="30" value="{{Auth::user()->username}}" required>
                           </div>
                       </div>
                       <div class="col-md-6">
                           <div class="form-group">
                             <label>Register Pada</label>
                             <input type="text" name="created_at" class="form-control" placeholder="" maxlength="30" value="{{Auth::user()->created_at}}" required readonly>
                           </div>
                       </div>
                     </div>

                   <button type="submit" class="btn btn-info btn-fill pull-right">Update</button>
                   <div class="clearfix"></div>
                </form>
             </div>
          </div>
       </div>
         <div class="col-md-4" style="border:1px solid lightgrey; padding:15px; border-radius:1%; position: relative;left:10px;">
             <div class="card card-user">
                 <div class="image" style="background:aqumarine;">
                   <center><img src="{{asset('assets')}}/images/users.jpg" style="width:170px;" alt="..."/></center>
                 </div>
                 <div class="content" style="padding-top:10px;">
                   <div class="author"></div>
                   <p class="description text-center"><b>Nama : {{ Auth::user()->nama}}</b><br /><b>Username : {{ Auth::user()->username}} <br> Register pada : {{ Auth::user()->created_at}}</b>
                   </textarea>
                   </p>
                   <center><a href="{{url('/history')}}" class="btn btn-info btn-md btn-fill">Check History</a></center>
                 </div>
             </div>
         </div>
    </div>
</div><br><br><br>
@endsection
