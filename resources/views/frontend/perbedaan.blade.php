@extends('templates.frontend')


@section('content')
<div class="container" >
  <div class="row">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h1>Perbedaan TOEIC & TOEFL</h1>
      </div>
      <div class="panel-body">

        <h4><b>Apa beda TOEIC dan TOEFL ?</b></h4><br>
        <p>Secara sederhana, TOEIClebih untuk sertifikat pengakuan bahwa kamu sudah siap masuk dunia kerja dengan kemampuan Bahasa Inggris memenuhi standar.<br>
          <br>Sedangkan TOEFL beda lagi.<br><br>
          TOEFL ditujukan untuk siswa yang ingin melanjutkan pendidikan ke luar negeri.TOEFL lebih ke arah akademis.
          Jadi, kalau kamu punya sertifikat TOEFL, maka kamu mempunyai bukti untuk kecakapan Bahasa Inggris yang kamu miliki.
        </p><br>

        <h4> <b>Standar Penilaian</b> </h4><br>
        <p>
          Kedua tes ini, baik TOEIC maupun TOEFL memiliki standar penilaian yang berbeda. Jadi, nilai tes TOEFL tidak bisa diselaraskan dengan nilai tes TOEIC.
          Umumnya, TOEFL memiliki perbedaan standar nilai tergantung dimana Anda melakukan tes.
          Sedangkan TOEIC, menggunakan standar internasional yang sama di manapun Anda melakukan tes.
        </p>

        <p>
          Skor TOEIC dimulai dari angka 400 hingga 990. TOEIC memiliki dua macam tes yaitu tes reading dan listening dengan rentang skor 10-990.
          Dan terdiri dari enam level yaitu :<br>

          <br><span style="padding-left:30px;">•Level 0/0+ Novice (skor 10-250)
          <br><span style="padding-left:30px;">•Level 1 Elementary (skor 255-400)
          <br><span style="padding-left:30px;">•Level 1+ Intermediate (skor 405-600)
          <br><span style="padding-left:30px;">•Level 2 Basic Working Proficiency (skor 605-780)
          <br><span style="padding-left:30px;">•Level 2+ Advance Working Profiency (skor 785-900)
          <br><span style="padding-left:30px;">•Level 3/3+ General Professional Proficiency (skor 905-990).<br><br>
          Sedangkan TOEFL memiliki tiga macam pilihan tes yang juga memiliki rentang skor dan materi tes nya masing-masing.<br>
          Tiga pilihan tes tersebut di antaranya adalah:
        </p><br>

        <h4> <b>1. PBT (Paper Based Test)</b> </h4><br>
        <p>
          TOEFL PBT adalah tes TOEFL yang dikerjakan di atas kertas. Materi tes di dalam TOEFL PBT adalah listening, reading comprehension
          dan structure and written expression.Untuk TOEFL PBT ini memiliki rentang skor mulai dari 310 hingga 677. Jenis tes TOEFL ini
          biasanya digunakan di tempat-tempat yang masih minim dalam hal komputer dan jaringan internet.
        </p><br>

        <h4> <b>2. CBT (Computer Based Test)</b> </h4><br>
        <p>
          TOEFL CBT adalah tes TOEFL yang dikerjakan di komputer, jadi media yang digunakan tidak lagi berbentuk kertas.
          TOEFL dengan jenis ini menggunakan semacam perangkat lunak atau software yang sudah dipersiapkan. Sehingga walaupun tidak memiliki jaringan internet,
          tes TOEFL dengan jenis ini masih bisa digunakan. Di Indonesia sendiri, TOEFL dengan jenis ini masih sangat jarang digunakan,
          dan masih banyak menggunakan TOEFL paper based test. Rentang skor untuk TOEFL jenis in adalah 30 hingga 300.
          Materi tes yang ada di dalam TOEFL CBT adalah listening, reading comprehension, structure and written expression, dan juga writing.
        </p><br>

        <h4> <b>3. IBT (Internet Based Test)</b> </h4><br>
        <p>
          TOEFL Internet Based Test atau TOEFL IBT juga banyak dikenal dengan NG-TOEFL atau Next Generation TOEFL.
          Jenis tes TOEFL ini merupakan jenis TOEFL terbaru yang dikembangkan oleh ETS atau Educational Testing Service sebagai
          lembaga yang mengfasilitasi tes TOEFL di seluruh dunia. Media yang digunakan dalam TOEFL IBT sama seperti
          TOEFL CBT yaitu dengan menggunakan komputer. Namun bedanya, jika TOEFL CBT menggunakan software, TOEFL IBT menggunakan jaringan internet.
          Peserta tes akan terhubung dengan database ETS, sehingga jawaban para peserta langsung terekam secara otomatis di dalam database ETS.
        </p>

      </div>
    </div>
  </div>
</div>
@endsection
