@extends('templates.frontend')


@section('content')
<div class="container">
  <div class="row">
    <div class="panel panel-default ">
      <div class="panel-heading">
        <h3> Form Tes</h3>
      </div>
      <form action="{{ URL('/validasiformtes')}}" method="POST" target="_blank" autocomplete="off">
          {{ csrf_field() }}<br>

        <div class="row" style="padding-left:20px; padding-right:20px;">
          <div class="col-md-6">
            <label>Nama Tester</label>
            <input type="text" name="nm_tester" class="form-control" required placeholder="Masukkan Nama" required>
          </div>
          <div class="col-md-6" >
            <label>Email</label>
            <input type="email" name="email" class="form-control" placeholder="Masukkan Email" required>
          </div>
        </div><br>

        <div class="row"  style="padding-left:20px; padding-right:20px;" required>
          <div class="col-md-6">
            <label>Tanggal Lahir</label>
              <input type="date" name="tgl_lahir" class="form-control" required>
          </div>
          <div class="col-md-6">
            <label>No Telepon</label>
              <input type="number" name="no_telepon" class="form-control" required>
          </div>
         </div><br>

         <div class="row"  style="padding-left:20px; padding-right:20px;">
            <div class="col-md-6">
              <label >Alasan</label>
              <textarea name="alasan" class="form-control"  required></textarea>
            </div>
         </div><br>

         <div class="panel-footer">
            <button type="submit" class="btn btn-primary  btn-save pull-right" style="margin-left:5px;"><i class="glyphicon glyphicon-floppy-disk"> </i> Submit</button>&nbsp
            <button type="reset" class="btn btn-raised btn-default pull-right"><i class="glyphicon glyphicon-refresh"></i> Reset</button><br><br>
         </div>
       </form>
     </div>
   </div>
 </div>
@endsection
