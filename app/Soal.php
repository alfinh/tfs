<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Soal extends Model
{
  protected $table='tbl_soal';

  protected $primaryKey = 'id_soal';

  public $timestamps = false;


  protected $fillable = [
    'id_soal','tipe_soal','soal_tulisan','soal_tulisan',
    'soal_gambar','soal_suara','jawaban_a','jawaban_b',
    'jawaban_c','jawaban_d','jawaban_asli','keterangan','waktu_pembuatan','id_paket'
  ];
}
