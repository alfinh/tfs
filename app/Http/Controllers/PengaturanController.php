<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Auth;
use App\User;
use Illuminate\Http\Request;

class PengaturanController extends Controller
{
  public function ubahpassword(Request $request){

  $newpass = $request->input('passwordbaru');
  $confirmpass = $request->input('passwordconfirm');

  if($newpass == $confirmpass){
    $user= Auth::user();
    $user->password = bcrypt($newpass);
    $user->save();

    $after_savee = [
        'alert' => 'success',
        'icon'  => 'check',
        'title' => 'Berhasil !',
        'text-1' => 'Password Anda ',
        'text-2' => 'Telah diubah !'
    ];

    return redirect()->back()->with('after_savee', $after_savee);

  }else{
    $after_savee = [
        'alert' => 'danger',
        'icon'  => 'attention',
        'title' => 'kesalahan !',
        'text-1' => 'Konfirmasi Password ',
        'text-2' => 'Tidak sesuai !'
    ];
      return redirect()->back()->with('after_savee', $after_savee);
  }

 }

 public function editakun(Request $request){
   $id = Auth::user()->id;
   $data = User::findOrFail($id);
       $data->nama = $request->input('nm_user');
       $data->username = $request->input('username');
       $data->save();

       $after_save = [
           'alert' => 'success',
           'icon'  => 'check',
           'title' => 'Berhasil !',
           'text-1' => 'Profil Anda ',
           'text-2' => 'Telah diubah !'
       ];

       return redirect()->back()->with('after_save', $after_save);

 }

}
