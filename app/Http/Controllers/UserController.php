<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
use DataTables;
use Illuminate\Http\Request;

class UserController extends Controller
{
  public function user()
   {

    return view('backend/userregister');
   }


  public function json(Request $request){
  if($request->get('tgl_mulai') && $request->get('tgl_akhir'))
    {
        $tgl_mulai  = $request->get('tgl_mulai');
        $tgl_akhir    = $request->get('tgl_akhir');
        $user = User::where('isadmin','=',0)->whereBetween('created_at',[$tgl_mulai,$tgl_akhir])->get();
        return Datatables::of($user)
        ->addColumn('action', function($user){
        return '<a href="delete_user/'.$user->id.'" class="btn btn-sm btn-danger delete" id=""><i class="glyphicon glyphicon-remove"></i> </a>';

        })

        ->make(true);
    }else{
        $user = User::where('isadmin','=',0);
        return Datatables::of($user)
        ->addColumn('action', function($user){
        return '<a href="delete_user/'.$user->id.'" class="btn btn-sm btn-danger delete" id=""><i class="glyphicon glyphicon-remove"></i></a>';

        })

        ->make(true);
    }
  }

  public function destroy($id) {

             DB::delete('delete from tbl_tester where id_user = ?',[$id]);
             DB::delete('delete from users where id = ?',[$id]);
             $after_save = [
                       'alert' => 'success',
                       'icon' => 'check',
                       'title' => 'Berhasil ! ',
                       'text-1' => 'Data Nilai ',
                       'text-2' => 'Telah dihapus.'
                   ];
             return redirect('/user')->with('after_save', $after_save);

  }
}
