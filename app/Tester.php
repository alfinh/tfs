<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tester extends Model
{
  protected $table='tbl_tester';

  protected $fillable = [
    'id_tester','id_user','id_paket',
      'nilai','waktu_mengerjakan','jawaban'
  ];

    public $timestamps = false;
}
