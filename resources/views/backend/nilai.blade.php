@extends('templates/backend')

@section('judul')
Nilai
@endsection

@section('content')
<div class="main">
  <!-- MAIN CONTENT -->
  <div class="main-content">
    <div class="container-fluid">
      <!-- OVERVIEW -->
    <div class="panel panel-default">
      <div class="panel-heading">
      <span style="font-size:22px;">  Data Nilai</span>
      </div>
      <br>
<!-- /.panel-heading -->
         {{-- part alert --}}
                @if (Session::has('after_save'))
                    <div class="col-md-12">
                        <div class="alert alert-dismissible alert-{{ Session::get('after_save.alert') }}">
                          <i class="pe-7s-{{ Session::get('after_save.icon') }}" style="font-size:30px; position:relative; top:8px;"></i>
                          <button type="button" class="close" data-dismiss="alert">×</button>
                          <strong>{{ Session::get('after_save.title') }}</strong>
                          <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_save.text-1') }}</a> {{ Session::get('after_save.text-2') }}
                        </div>
                    </div>
                @endif
          {{-- end part alert --}}
    <div class="panel-body">
      <div class="row" style="padding-top:10px;">
          <!-- <form id="search-form" class="form-inline" method="POST" role="form" action=""> -->
            {{csrf_field()}}
                   <div class="col-sm-7">
                    <div class="form-group">
                     <label>Pencarian data berdasarkan tanggal testing</label>
                     <div class="input-group date">
                        <span class="glyphicon glyphicon-th-list" style="font-size:27px; position:relative; top:7px; "></span>
                        <input type="date"id="tgl_mulai"placeholder="masukkan tanggal Awal"  class="datepicker" name="tgl_mulai"/>&nbsp&nbsp
                        <span class="glyphicon glyphicon-th-list" style="font-size:27px; position:relative; top:7px;"></span>
                        <input type="date" id="tgl_akhir" placeholder="masukkan tanggal Akhir" class="datepicker" name="tgl_akhir" >&nbsp&nbsp&nbsp
                        <button type="button" id="btn-cari" class="btn btn-default">Cari</button>
                        <!-- <input type="submit" id="btn-cari" class="btn btn-default" name="submit" value="cari" style="margin-bottom:5px;">&nbsp -->
                   </div>
                </div>
             </div>
          <!-- </form> -->
        </div>
       <div class="table-responsive">
         <table  class="table table-striped table-bordered table-hover " id="tbl-nilai" >
           <thead>
              <tr>
                <td><b>No</b></td>
                <td><b>Nilai</b></td>
                <td><b>Banyak Benar</b></td>
                <td><b>Banyak Salah</b></td>
                <td><b>Id Tester</b></td>
                <td><b>Waktu</b></td>
                <td><b>ACTION</b></td>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
    @endsection

    @push('scripts')
    <script type="text/javascript">
    var t_nilai;
    $(function() {
      t_nilai = $('#tbl-nilai').DataTable({
            pagingType:"full_numbers",
            processing: true,
            "language": {
            processing: "<img src='{{asset('assets/images/2 (2).gif')}}'> "},
            serverSide: true,
            ajax:{
              url:'{{ route("nilai.json") }}',
              data: function (d) {
                d.tgl_mulai       = $('input[name="tgl_mulai"]').val();
                d.tgl_akhir         = $('input[name="tgl_akhir"]').val();
            }
          },
            columns: [
                { data: null, orderable: false},
                { data: 'nilai', name: 'nilai' },
                { data: 'banyak_benar', name:'banyak_benar'},
                { data: 'banyak_salah', name:'banyak_salah'},
                { data: 'id_tester', name: 'id_tester'},
                { data: 'ket_waktu', name: 'ket_waktu' },
                { data: 'action', orderable:false, searchable:false }
              ],
              dom: 'B<"toolbar">ifrtlp',
              "rowCallback": function (nRow, aData, iDisplayIndex) {
               var oSettings = this.fnSettings ();
               $("td:first", nRow).html(oSettings._iDisplayStart+iDisplayIndex +1);
               return nRow;
             }
     } );

     $("#btn-cari").on('click', function(e){
       t_nilai.ajax.reload();
     });

     $(document).on('click', '.delete', function(){
           if(confirm("Are you sure you want to Delete this data?"))
           {
            alert('Record deleted successfully.'); window.location.href='/nilai';
           }
           else
           {
               return false;
           }
       });
    });
    </script>
    @endpush
