@extends('templates.frontend')


@section('content')
<div class="container" >
  <div class="row">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h1>Pengertian Umum</h1>
      </div>
      <div class="panel-body">
        <h4><b>Apa itu Tes TOEIC ?</b></h4><br>
        <p>Tes TOEIC adalah sebuah tes untuk menunjukkan kemampuan berkomunikasi dalam bahasa Inggris di dunia kerja atau ambil bagian dalam globalisasi.
          Tes ini hanya dilaksanakan di negara-negara yang tidak berbahasa Ibu Bahasa Inggris.</p><br>

          <h4><b>Seberapa penting sertifikat TOEIC untuk kamu?</b></h4>
          <br>1.	Syarat mendapatkan pekerjaan impian
          <br>2.	Syarat kenaikan level kerja
          <br>3.	Syarat kelulusan untuk beberapa SMK dan Politeknik<br><br>

          <h4><b>Tentang TOEIC yang harus kamu tahu</b></h4>
          <br>1.	Pilihan ganda terdiri dari 200 pertanyaan
          <br>2.	Soal dikerjakan dalam 2 jam. Skor TOEIC dimulai dari angka 400 hingga 990.
          <br>3.	Ada enam level dalam TOEIC, yaitu :
          <br><span style="padding-left:30px;">• Level 0/0+ Novice (skor 10-250)</span>
          <br><span style="padding-left:30px;">• Level 1 Elementary (skor 255-400)</span>
          <br><span style="padding-left:30px;">• Level 1+ Intermediate (skor 405-600)</span>
          <br><span style="padding-left:30px;">• Level 2 Basic Working Proficiency (skor 605-780)</span>
          <br><span style="padding-left:30px;">• Level 2+ Advance Working Profiency ( skor 785-900)</span>
          <br><span style="padding-left:30px;">• Level 3/3+ General Professional Proficiency (skor 905-990).</span>
          <br>4. TOEIC memiliki dua macam tes yaitu tes reading dan listening dengan rentang skor 10-990.


      </div>
    </div>
  </div>
</div>
@endsection
