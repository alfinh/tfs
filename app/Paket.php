<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paket extends Model
{
  protected $table='tbl_paket';

  protected $primaryKey = 'id_paket';

  public $timestamps = false;


  protected $fillable = [
    'id_paket','paket','deskripsi_paket','waktu_pengerjaan',
    'created_at'
  ];
}
