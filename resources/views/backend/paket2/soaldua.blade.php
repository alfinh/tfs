@extends('templates/backend')

@section('judul')
Soal
@endsection

@push('css')
<style>
#foto{
  max-width: 200px;
  max-height: 100px;
}
#foto2{
    max-width: 200px;
    max-height: 100px;
  }
</style>
@endpush

@section('content')
<div class="main">
  <!-- MAIN CONTENT -->
  <div class="main-content">
    <div class="container-fluid">
      <!-- OVERVIEW -->
    <div class="panel panel-default">
<!-- /.panel-heading -->

    <div class="panel-body">
    <h2>PAKET ENGLISH 2</h2>
    <p>Jumlah Soal : {{$jumlahall}}</p>
    <p>Soal Reading : {{$jumlahreading}} </p>
    <p>Soal Listening : {{$jumlahlistening}}</p><hr>
    {{-- part alert --}}
           @if (Session::has('after_save'))
               <div class="col-md-12">
                   <div class="alert alert-dismissible alert-{{ Session::get('after_save.alert') }}">
                     <i class="pe-7s-{{ Session::get('after_save.icon') }}" style="font-size:30px; position:relative; top:8px;"></i>
                     <button type="button" class="close" data-dismiss="alert">×</button>
                     <strong>{{ Session::get('after_save.title') }}</strong>
                     <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_save.text-1') }}</a> {{ Session::get('after_save.text-2') }}
                   </div>
               </div>
           @endif
     {{-- end part alert --}}
    <a class="btn btn-xs btn-default" href="#" data-toggle="modal" data-target="#addForm">Tambah <i class="glyphicon glyphicon-plus"></i></a>
    <div class="row" style="padding-top:10px;">
                 <div class="col-sm-7">
                  <div class="form-group">
                   <label>Pencarian data berdasarkan tanggal buat</label>
                    <div class="input-group date">
                       <span class="glyphicon glyphicon-th-list" style="font-size:27px; position:relative; top:7px; "></span>
                       <input type="date"id="tgl_mulai"placeholder="masukkan tanggal Awal"  class="datepicker" name="tgl_mulai"/>&nbsp&nbsp
                       <span class="glyphicon glyphicon-th-list" style="font-size:27px; position:relative; top:7px;"></span>
                       <input type="date" id="tgl_akhir" placeholder="masukkan tanggal Akhir" class="datepicker" name="tgl_akhir" >&nbsp&nbsp&nbsp
                       <button type="button" id="btn-cari" class="btn btn-default">Cari</button>
                 </div>
              </div>
           </div>
        <!-- </form> -->
      </div>
    <!-- Add Modal Add SoalReading-->
    <div class="modal fade" tabindex="-1" id="addForm" role="dialog" data-backdrop="false">
           <div class="modal-dialog" role="document" >
               <div class="modal-content">
                   <!-- Modal Header -->
                   <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                       <span aria-hidden="true"> &times;</span>
                     </button>
                     <h3 class="modal-title"><b>Tambah Soal Paket 2</b></h3>
                   </div>
                   <form action="{{ URL('/validasisoal')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                     {{ csrf_field() }}
                     <input type="hidden" name="paket" value=2>
                     <div class="modal-body">
                       <div class="row" >
                         <div class="col-md-6">
                           <label>Tipe Soal</label>
                           <select name="tipe_soal" class="form-control">
                             <option value="reading">Reading</option>
                             <option value="listening">Listening</option>
                           </select>
                         </div>
                         <div class="col-md-6" >
                           <label>Soal Tulisan</label>
                           <textarea name="soal_tulisan" class="form-control" required></textarea>
                         </div>
                       </div>
                       <br>

                       <div class="row"  >
                         <div class="col-md-6">
                           <label>Soal Gambar</label>
                             <input type="file" name="soal_gambar" onchange="readURL(this)" class="form-control" id="foto_soal" >
                             <img id="foto"></i>
                         </div>
                         <div class="col-md-6">
                           <label>Soal Suara</label>
                             <input type="file" name="soal_suara" class="form-control"  readonly style="border:1px solid lightgrey;">
                         </div>
                       </div>
                       <br>

                       <div class="row"  >
                         <div class="col-md-6">
                           <label >Jawaban A</label>
                           <input type="text" name="jawaban_a" class="form-control" placeholder="jawaban pilihan a" required>
                         </div>
                         <div class="col-md-6">
                           <label >Jawaban B</label>
                           <input type="text" name="jawaban_b" class="form-control" placeholder="jawaban pilihan b" required>
                         </div>
                       </div>
                       <br>

                       <div class="row"  >
                         <div class="col-md-6">
                           <label >Jawaban C</label>
                           <input type="text" name="jawaban_c" class="form-control" placeholder="jawaban pilihan c" required>
                         </div>
                         <div class="col-md-6">
                           <label >Jawaban D</label>
                           <input type="text" name="jawaban_d" class="form-control" placeholder="jawaban pilihan d" >
                         </div>
                       </div>
                       <br>

                       <div class="row"  >
                         <div class="col-md-6">
                           <label >Jawaban Benar</label>
                           <input type="text" name="jawaban_asli" class="form-control" placeholder="jawaban yang benar" required>
                         </div>
                         <div class="col-md-6">
                           <label >Keterangan</label>
                           <textarea name="keterangan" class="form-control" required></textarea>
                         </div>
                       </div>
                    </div>

                    <div class="modal-footer">
                        <button type="reset" class="btn btn-raised btn-default btn-fill"><i class="glyphicon glyphicon-refresh"> </i> Reset</button>
                        <button type="submit" class="btn btn-primary btn-save btn-fill  pull-right"><i class="glyphicon glyphicon-floppy-disk"></i> Submit</button>
                    </div>
               </form>
          </div>
      </div>
  </div>
    <div class="table-responsive">
    <table  class="table table-striped table-bordered table-hover " id="tbl-soaldua" >
    <thead>
        <tr>
          <td><b>No</b></td>
          <td><b>Tipe Soal</b></td>
          <td><b>Soal Tulisan</b></td>
          <td><b>Soal Gambar</b></td>
          <td><b>Soal Suara</b></td>
          <td><b>Jawaban Asli</b></td>
          <td><b>Waktu Pembuatan</b></td>
          <td><b>ACTION</b></td>
          </tr>
      </thead>
        </table>
      </div>
    </div>
    </div>
  </div>
</div>
</div>
<!-- Add Modal Add SoalReading-->
<div class="modal fade" tabindex="-1" id="editForm" role="dialog" data-backdrop="false">
       <div class="modal-dialog" role="document" >
           <div class="modal-content">
               <!-- Modal Header -->
               <div class="modal-header">
                 <h3 class="modal-title"><b>Edit Soal Paket 2</b></h3>
               </div>
               <form action="/updatesoal" method="POST" autocomplete="off" enctype="multipart/form-data">
                 {{ csrf_field() }}
                 <input type="hidden" name="paket" value=2>
                 <div class="modal-body">
                   <div class="row" >
                     <div class="col-md-12">
                       <label>Kode Soal</label>
                       <input type="number" name="id_soal" class="form-control" id="id_soal" readonly style="border:1px solid lightgrey;">
                     </div>
                   </div>
                   <br>

                   <div class="row" >
                     <div class="col-md-6">
                       <label>Tipe Soal</label>
                       <select name="tipe_soal" class="form-control" id="tipe">
                         <option value="reading">Reading</option>
                         <option value="listening">Listening</option>
                       </select>
                     </div>

                     <div class="col-md-6" >
                       <label>Soal Tulisan</label>
                       <textarea name="soal_tulisan" class="form-control" id="soal_tulisan" required></textarea>
                     </div>
                   </div>
                   <br>

                   <div class="row"  >
                     <div class="col-md-6">
                       <label>Soal Gambar</label>
                         <input type="file" name="soal_gambar" onchange="readURLL(this)" class="form-control" id="foto_soal2"  >
                         <img id="foto2"></i>
                     </div>
                     <div class="col-md-6">
                       <label>Soal Suara</label>
                         <input type="file" name="soal_suara" class="form-control" readonly style="border:1px solid lightgrey;">
                     </div>
                   </div>

                   <div class="row"  >
                     <div class="col-md-6">
                       <label >Jawaban A</label>
                       <input type="text" name="jawaban_a" class="form-control" id="jawaban_a" placeholder="jawaban pilihan a" required>
                     </div>
                     <div class="col-md-6">
                       <label >Jawaban B</label>
                       <input type="text" name="jawaban_b" class="form-control" id="jawaban_b" placeholder="jawaban pilihan b" required>
                     </div>
                   </div>
                   <br>

                   <div class="row"  >
                     <div class="col-md-6">
                       <label >Jawaban C</label>
                       <input type="text" name="jawaban_c" class="form-control" id="jawaban_c" placeholder="jawaban pilihan c" required>
                     </div>
                     <div class="col-md-6">
                       <label >Jawaban D</label>
                       <input type="text" name="jawaban_d" class="form-control" id="jawaban_d" placeholder="jawaban pilihan d" >
                     </div>
                   </div>

                   <br>
                   <div class="row"  >
                     <div class="col-md-6">
                       <label >Jawaban Benar</label>
                       <input type="text" name="jawaban_asli" class="form-control" id="jawaban_asli" placeholder="jawaban yang benar" required>
                     </div>
                     <div class="col-md-6">
                       <label >Keterangan</label>
                       <textarea name="keterangan" class="form-control" id="keterangan" required></textarea>
                     </div>
                   </div>
              </div>

              <div class="modal-footer">
                  <button type="button" class="btn btn-raised btn-default btn-fill " data-dismiss="modal" aria-label="Close"><i class="glyphicon glyphicon-arrow-left"> </i> Cancel</button>
                  <button type="submit" class="btn btn-primary btn-save btn-fill  pull-right"><i class="glyphicon glyphicon-floppy-disk"></i> Submit</button>
            </div>
         </form>
      </div>
   </div>
</div>
    @endsection

    @push('scripts')
    <script type="text/javascript">

    var t_dua ;
    $(function(){
       t_dua = $('#tbl-soaldua').DataTable({
            pagingType:"full_numbers",
            processing: true,
            "language": {
            processing: "<img src='{{asset('assets/images/2 (2).gif')}}'> "},
            serverSide: true,
            ajax:{
              url:'{{ route("paketdua.json") }}',
              data: function (d) {
                d.tgl_mulai       = $('input[name="tgl_mulai"]').val();
                d.tgl_akhir         = $('input[name="tgl_akhir"]').val();
            }
          },
            columns: [
                { data: null, orderable: false},
                { data: 'tipe_soal', name: 'tipe_soal' },
                { data: 'soal_tulisan', name: 'soal_tulisan' },
                { data: 'soal_gambar', name: 'soal_gambar',render: function ( data, type, full ) {
                     return '<img src="uploads/file/' + data + '" height="100px" width="125px"></i>'; }},
                { data: 'soal_suara', name: 'soal_suara',render: function ( data, type, full ) {
                   return '<audio src="uploads/file/'+ data +'" controls> '; }},
                { data: 'jawaban_asli', name: 'jawaban_asli' },
                { data: 'waktu_pembuatan', name: 'waktu_pembuatan' },
                { data: 'action', orderable:false, searchable:false }
              ],
              dom: 'B<"toolbar">ifrtlp',
              "rowCallback": function (nRow, aData, iDisplayIndex) {
               var oSettings = this.fnSettings ();
               $("td:first", nRow).html(oSettings._iDisplayStart+iDisplayIndex +1);
               return nRow;
             },

     } );

     $("#btn-cari").on('click', function(e){
       t_dua.ajax.reload();
     });

     $('#tbl-soaldua tbody').on( 'click', '#editclick', function () {
      var data = t_dua.row( $(this).parents('tr') ).data();
      $('#tipe option[value="'+  data['tipe_soal'] +'"').attr("selected",true);
      $("textarea#soal_tulisan").val(data['soal_tulisan']);
      $("#foto2").attr("src","uploads/file/"+data['soal_gambar']);
      $("#id_soal").val(data['id_soal'])
      $("#jawaban_a").val(data['jawaban_a']);
      $("#jawaban_b").val(data['jawaban_b']);
      $("#jawaban_c").val(data['jawaban_c']);
      $("#jawaban_d").val(data['jawaban_d']);
      $("#jawaban_asli").val(data['jawaban_asli']);
      $('textarea#keterangan').val(data['keterangan']);
    });

     $(document).on('click', '.delete', function(){
           if(confirm("Are you sure you want to Delete this data?"))
           {
            alert('Record deleted successfully.'); window.location.href='/paketdua';
           }
           else
           {
               return false;
           }
       });
    });
    function readURL(input) {

              var file = document.getElementById('foto_soal').files[0];

              if(file && file.size < 1000000) { //1mb size file validation
                  var file = document.querySelector("#foto_soal");
                   if ( /\.(jpe?g|png|gif)$/i.test(file.files[0].name) === false )
                   {
                     alert("Error : File type must be ( jpg,jpeg,png,gif ) !");
                     input.value = null; // Clear the field.
                   }else{
                      if (input.files && input.files[0]) {
                          var reader = new FileReader();

                          reader.onload = function (e) {
                              $('#foto')
                                  .attr('src', e.target.result);
                          };

                          reader.readAsDataURL(input.files[0]);
                     }
                  }

              } else {

                alert("Error : File size over 1 mb !"); // Do your thing to handle the error.
                input.value = null; // Clear the field.
                  }
                }

        function readURLL(input) {

                  var file = document.getElementById('foto_soal2').files[0];

                  if(file && file.size < 1000000) { //1mb size file validation
                      var file = document.querySelector("#foto_soal2");
                       if ( /\.(jpe?g|png|gif)$/i.test(file.files[0].name) === false )
                       {
                         alert("Error : File type must be ( jpg,jpeg,png,gif ) !");
                         input.value = null; // Clear the field.
                       }else{
                          if (input.files && input.files[0]) {
                              var reader = new FileReader();

                              reader.onload = function (e) {
                                  $('#foto2')
                                      .attr('src', e.target.result);
                              };

                              reader.readAsDataURL(input.files[0]);
                         }
                      }

                  } else {

                    alert("Error : File size over 1mb !"); // Do your thing to handle the error.
                    input.value = null; // Clear the field.
                      }
                    }


    </script>
    @endpush
