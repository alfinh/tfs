@extends('templates/user')

@section('content')

<div class="container"><br><br><br><br>
   <div class="row">
      <div class="col-sm-12">
        <div class="panel">
          <div class="panel-heading" style="border:1px solid #eee"><h2>Materi Listening</h2></div>
          <div class="panel-body">
             <!--begin tabs going in narrow content -->
             <ul class="nav nav-tabs sidebar-tabs" id="sidebar" role="tablist" style="margin-top:3px;">
                <li class="active"><a href="#tab-1" role="tab" data-toggle="tab">Photograph</a></li>
                <li><a href="#tab-2" role="tab" data-toggle="tab">QuestionResponse</a></li>
                <li><a href="#tab-3" role="tab" data-toggle="tab">Conservation</a></li>
                <li><a href="#tab-4" role="tab" data-toggle="tab">Short talk</a></li>
             </ul><!--/.nav-tabs.sidebar-tabs -->
             <!-- Tab panes -->
             <div class="tab-content">
                <div class="tab-pane active" id="tab-1" style="color:grey; font-family:Calibri; font-size:17px;" >
                  <span style="font-size:22px; color:grey; font-family:Calibri;"><b>Materi Listening (Photograph)</b></span><br>
                  <span  style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px;">
                    Pada setiap pertanyaan anda akan melihat gambar dan mendengar 4 (empat) pernyataan.
                    Setiap pernyataan hanya diperdengarkan 1 (satu) kali. Semua pernyataan tidak ditampilkan.
                    Jadi anda harus mendengarkan dengan seksama untuk dapat mengerti apa yang disampaikan oleh pembicara.</div></span>
                    <br>
                    <span style="font-size:22px; color:grey; font-family:Calibri;"><b>Tips and Trick (Photograph)</b></span><br>
                  <span  style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px;">
                    • Perhatikan gambar secara cepat sebelum empat pilihan jawaban diperdengarkan.
                    <br>• Pertanyaan mengenai gambar: who, what, where, when.
                    <br>• Jawab pertanyaan dengan cepat karena pilihan jawaban tidak akan diperdengarkan dua kali.
                    <br>• Tiga pilihan jawaban yang salah memiliki karakteristik sebagai berikut :
                    <br>
                    - Mengandung jawaban yang berisi sebagian pernyataan yang salah.<br>
                    - Berisi kata yang tidak mengacu kepada konteks gambar yang diperlihatkan.<br>
                    - Mengandung kata-kata yang memiliki bunyi yang serupa.
                    <br></div></span><br>

                    <p>
                      Ini adalah contoh dari Photograph<br>
                      <br>Amati gambar berikut dan dengarkan audio dengan seksama
                    </p>
                      <img src="assets/images/photograph.jpg" alt="" style="width:500px; height:300px;"><br><br>
                      <audio src="assets/audio/photograph.mpeg" controls>
                      </audio>

                      <p>
                        A. Traffic is building up on the motorway</p>
                      <p>B. There are more lorries on this motorway than cars </p>
                      <p style="color:red;">C. Traffic is flowing freely on the motorway</p>
                      <p>D. The vehicles are travelling too close to one another on the motorway</p>

                </div><!--/.tab-pane -->
                <div class="tab-pane" id="tab-2" style="color:grey; font-family:Calibri; font-size:17px;" >
                    <span style="font-size:22px; color:grey; font-family:Calibri;"><b>Materi Listening (Question-Response)</b></span><br>
                    <br>
                    <span  style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px;">
                      Pada bagian ini anda akan mendengarkan sebuah pertanyaan atau pernyataan dalam Bahasa Inggris dan diikuti dengan tiga respon yang
                      juga disampaikan dalam Bahasa Inggris. Baik pertanyaan atau pernyataan dan juga respon hanya akan diperdengarkan satu kali.
                      Semua keterangan tersebut tidak ditampilkan, jadi anda harus mendengarkan dengan seksama untuk dapat mengerti
                      apa yang disampaikan oleh pembicara tersebut. Anda harus memilih respon yang paling tepat untuk setiap pertanyaan.</div></span>
                      <br>
                    <span style="font-size:22px; color:grey; font-family:Calibri;"><b>Tips and Trick (Question Response)</b></span><br>
                    <span  style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px;">
                      • Perhatikan kata-kata yang memiliki bunyi serupa namun memiliki arti yang berbeda.<br>
                      • Perhatikan : who, what, when, where, why, how yang menanyakan tentang informasi.<br>
                      • Perhatikan pertanyaan dengan question tag yang membutuhkan jawaban Yes/No.<br>
                      • Perhatikan pertanyaan yang tetap memerlukan jawaban.<br>
                      • Pilihliah jawaban yang paling tepat dengan cepat, jika anda merasa ragu, jangan kosongkan lembar
                        jawaban karena tidak ada pengurangan poin atas jawaban yang salah.<br>
                      <br>
                      - Mengandung jawaban yang berisi sebagian pernyataan yang salah.<br>
                      - Berisi kata yang tidak mengacu kepada konteks gambar yang diperlihatkan.<br>
                      - Mengandung kata-kata yang memiliki bunyi yang serupa.</div></span><br><br>

                      <p>Dengarkan audio dibawah dengan seksama</p>
                      <audio src="assets/audio/qresponse.mpeg" controls>
                      </audio>

                        <p  style="color:red;">
                          A. I think he ordered the pasta</p>
                        <p>B. He told me to type the report</p>
                        <p>C. At six p.m in the dining hall</p>

                  </div><!--/.tab-pane -->
                  <div class="tab-pane" id="tab-3" style="color:grey; font-family:Calibri; font-size:17px;" >
                    <span style="font-size:22px; color:grey; font-family:Calibri;"><b>Materi Listening (Short Conversation)</b></span><br>
                    <br>
                    <span  style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px;">
                        Pada bagian ini anda kan mendengarkan dialog singkat antara dua orang. Dialog tidak ditampilkan.
                        Anda akan mendengarkan dialog tersebut satu kali, karenanya anda harus mendengarkan dengan seksama untu
                        mengerti apa yang disampaikan oleh kedua pembicara tersebut</div></span>
                      <br>
                    <span style="font-size:22px; color:grey; font-family:Calibri;"><b>Tips and Trick (Short Conversation)</b></span><br>
                    <span  style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px;">
                      • Dalam menyimak percakapan, buatlah pertanyaan-pertanyaan bantuan untuk menganalisa isi percakapan,
                        sehingga anda akan lebih mudah dalam menjawab pertanyaan yang diajukan selanjutnya.<br>
                      • Telitilah dalam membaca pilihan jawabn, karena mungkin ada jawaban yang benar namun tidak berhubungan dengan pertanyaan yang diajukan.<br>
                      • Jangan mendengarkan lalu membaca. Bacalah dahulu baru mendengarkan.<br>
                      • Waspadailah hal-hal berikut :<br>
                      <br>
                      - Kata-kata yang memiliki bunyi serupa, namun memiliki arti yang berbeda.<br>
                      - Kata-kata yang tidak tepat.</div></span><br><br>

                      <p>Dengarkan audio dibawah dengan seksama</p>
                      <audio src="assets/audio/conversation.mpeg" controls>
                      </audio>
                        <p>1. Where is the conversation taking place?</p>
                        <p>
                          A. In a church</p>
                        <p  style="color:red;">B. In an office</p>
                        <p>C. In a classroom</p>
                        <p>D. in park</p><br>

                        <p>2. What problem does the woman have?</p>
                        <p  >
                          A. She will be late for work</p>
                        <p>B. She cannot make the meeting</p>
                        <p style="color:red;">C. She is struggling with her presentation</p>
                        <p>D. She worked late yesterday</p><br>

                        <p>3. What does the man offer</p>
                        <p  style="color:red;">
                          A. To help</p>
                        <p>B. To write her report</p>
                        <p>C. To get coffee</p>
                        <p>D. To make copies</p>


                  </div><!--/.tab-pane -->
                  <div class="tab-pane" id="tab-4" style="color:grey; font-family:Calibri; font-size:17px;" >
                    <span style="font-size:22px; color:grey; font-family:Calibri;"><b>Materi Listening (Short Talk)</b></span><br>
                    <br>
                    <span  style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px;">
                    Pada bagian ini, anda akan mendengarkan beberapa percakapan singkat. Semua ini akan diperdengarkan satu kali.
                    Percakapan ini tidak akan ditampilkan, jadi anda harus mendengarkan dengan seksama untuk
                    dapat mendengarkan dan mengingat apa yang disampaikan.</div></span>
                      <br>
                    <span style="font-size:22px; color:grey; font-family:Calibri;"><b>Tips and Trick (Short Talk)</b></span><br>
                    <span  style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px;">
                      • Bacalah jawaban dan pilihan jawaban sebelum narasi diperdengarkan.<br>
                      • angan mendengarkan lalu membaca. Bacalah terlebih dahulu baru mendengarkan.<br>
                      • Dengarkan bagian awal narasi. Bagian ini dapat memberikan anda gambaran tentang jenis informasi apa yang diberikan oleh narasi.<br>
                      • Mulailah menjawab pertanyaan begitu narasi selesai diperdengarkan.<br>
                      • Waspadailah hal-hal berikut : <br>
                      <br>
                      - Kata-kata yang memiliki bunyi serupa, namun memiliki arti yang berbeda.<br>
                      - Kata-kata yang tidak tepat.<br>
                      - Susunan kata yang tidak tepat.<br>
                      - Angka pengecoh, seperti waktu, tanggal, atau jumlah orang.</div></span><br><br>

                      <p>Dengarkan audio dibawah dengan seksama</p>
                      <audio src="assets/audio/talk.mpeg" controls>
                      </audio>
                        <p>1. Who is the intended audience?</p>
                        <p>
                          A. The general public</p>
                        <p>B. University students</p>
                        <p style="color:red;">C. Newspaper employees</p>
                        <p>D. Magazine advertisers</p><br>

                        <p>2. Who most likely is the speaker</p>
                        <p style="color:red;">
                          A. Publisher</p>
                        <p>B. Reporter</p>
                        <p>C. Photographer</p>
                        <p>D. Customer</p><br>

                        <p>3. What will begin tomorrow?</p>
                        <p>
                          A. A new TV program</p>
                        <p  style="color:red;">B. A news website</p>
                        <p>C. A newspaper</p>
                        <p>D. A recession</p>


                </div><!--/.tab-pane -->
              </div><!--/.tab-content -->

          </div> <!--/.col-sm-4 -->
        <br>
      </div>
    </div><!--/.row -->
  </div><!--/.container -->
</div>

@endsection
