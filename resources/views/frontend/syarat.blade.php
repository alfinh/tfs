@extends('templates.frontend')


@section('content')
<div class="container" >
  <div class="row">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h1>Syarat</h1>
      </div>
      <div class="panel-body"><br>
        <h4> <b>Prosedur & Persyarat</b> </h4><br>
        <p>
          Untuk mengikuti sebuah ujian TOEIC, kita tidak disyaratkan harus mengantongi tingkat pendidikan tertentu serta tidak ada batasan usia,
          siapa saja dapat mengikuti ujian ini. Berbeda dengan ujian kompetensi bahasa Inggris lainnya yang hanya dapat diikuti satu kali,
          ujian TOEIC dapat kamu ikuti berulang kali sepanjang kamu mungkin ingin meningkatkan nilai TOEIC kamu.
        </p><br>

        <p>
          Biaya ujian TOEIC beragam berdasarkan di negara mana tes tersebut dilaksanakan. Biaya ini dibayarkan sewaktu melakukan pendafratan.
          Namun, tes TOEIC yang dilaksanakan oleh perusahaan di tempat kita bekerja, harga ditentukan oleh perusahaan tersebut.
          Jika karena alasan tertentu, kamu harus melakukan pembatalan, kamu perlu menghubungi kantor ETC setempat.
          Meskipun kamu telah membatalkan ujian TOEIC, biaya yang telah dibayarkan tidak dapat ditarik kembali.
        </p>

      </div>
    </div>
  </div>
</div>
@endsection
