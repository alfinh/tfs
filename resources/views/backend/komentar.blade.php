@extends('templates/backend')

@section('judul')
Komentar
@endsection

@section('content')
<div class="main">
  <!-- MAIN CONTENT -->
  <div class="main-content">
  <div class="container-fluid">
      <!-- OVERVIEW -->
  <div class="panel panel-default">
    <div class="panel-body">
      <h3 style="margin-top:10px;">Most Recent Records </h3><hr>
        {{-- part alert --}}
               @if (Session::has('after_save'))
                 <div class="col-md-12">
                     <div class="alert alert-dismissible alert-{{ Session::get('after_save.alert') }}">
                       <i class="pe-7s-{{ Session::get('after_save.icon') }}" style="font-size:30px; position:relative; top:8px;"></i>
                       <button type="button" class="close" data-dismiss="alert">×</button>
                       <strong>{{ Session::get('after_save.title') }}</strong>
                       <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_save.text-1') }}</a> {{ Session::get('after_save.text-2') }}
                     </div>
                 </div>
               @endif
         {{-- end part alert --}}
        @foreach($komentar as $komen)
        <div style=" background:#eee; padding-top:10px; padding-right:10px;  padding-left:10px; padding-bottom:30px;  border:1px solid lightgrey; border-radius:1%; font-family:Calibri; word-wrap: break-word;">
          <p>
          <i><b> Pengirim : </b> {{ $komen->nm_pengirim }} <span class="pull-right"><i>{{ $komen->ket_waktu}}</i></span> <br><b>Kritik & Saran : </b>{{ $komen->komentar}}</i>
          <a href="delete_komentar/{{ $komen->id_komentar }}" class="btn btn-xs btn-danger pull-right delete" id="" style="margin-top:20px;"><i class="glyphicon glyphicon-remove"></i> Hapus</a>
          </p>
        </div><br>
        @endforeach
        <br>
      </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">

 $(document).on('click', '.delete', function(){
       if(confirm("Are you sure you want to Delete this data?"))
       {
        alert('Record deleted successfully.'); window.location.href='/komentar';
       }
       else
       {
           return false;
       }
   });
</script>
@endpush
