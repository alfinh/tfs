-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 03, 2019 at 09:49 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toeic`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2018_11_23_185308_create_table_soal', 1),
(3, '2018_11_23_190821_create_table_komentar', 1),
(4, '2019_01_05_060754_create_table_tester', 1),
(5, '2019_02_03_091549_create_table_paket', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_komentar`
--

CREATE TABLE `tbl_komentar` (
  `id_komentar` int(10) UNSIGNED NOT NULL,
  `nm_pengirim` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `komentar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ket_waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_komentar`
--

INSERT INTO `tbl_komentar` (`id_komentar`, `nm_pengirim`, `komentar`, `ket_waktu`) VALUES
(2, 'agyjon', 'check', '2019-02-05 23:01:38'),
(3, 'Akri SN', 'kampang', '2019-02-08 06:47:25'),
(4, 'Akri SN', 'paket 2 jancukk', '2019-02-11 08:07:13');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_paket`
--

CREATE TABLE `tbl_paket` (
  `id_paket` int(10) UNSIGNED NOT NULL,
  `paket` int(11) NOT NULL,
  `deskripsi_paket` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `waktu_pengerjaan` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_paket`
--

INSERT INTO `tbl_paket` (`id_paket`, `paket`, `deskripsi_paket`, `waktu_pengerjaan`, `created_at`) VALUES
(1, 1, 'Easy', 3600, '2019-02-05 03:27:22'),
(2, 2, 'Hard', 3600, '2019-02-05 03:27:22');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soal`
--

CREATE TABLE `tbl_soal` (
  `id_soal` int(10) UNSIGNED NOT NULL,
  `tipe_soal` enum('reading','listening') COLLATE utf8mb4_unicode_ci NOT NULL,
  `soal_tulisan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `soal_gambar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `soal_suara` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `jawaban_a` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jawaban_b` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jawaban_c` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jawaban_d` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jawaban_asli` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_paket` int(11) NOT NULL,
  `waktu_pembuatan` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_soal`
--

INSERT INTO `tbl_soal` (`id_soal`, `tipe_soal`, `soal_tulisan`, `soal_gambar`, `soal_suara`, `jawaban_a`, `jawaban_b`, `jawaban_c`, `jawaban_d`, `jawaban_asli`, `keterangan`, `id_paket`, `waktu_pembuatan`) VALUES
(1, 'reading', 'This is a general notice for all passengers: the restaurant in the main ....... is now open.', '-.png', '-', 'A. concord', 'B. conduit', 'C. concourse', 'D. condition', 'c', 'Incomplete Sentence', 1, '2019-02-05 03:37:45'),
(2, 'reading', 'The small cafe situated near platform 6 is now ....... free cups of tea to those waiting for the London train.', '-.png', '-', 'A. discharging', 'B. dispensing', 'C. offering', 'D. disposing', 'c', 'Incomplete Sentence', 1, '2019-02-05 03:39:24'),
(3, 'reading', 'Severe storms last night together with heavy rainfall means that some of the lines of the station are now .......', '-.png', '-', 'A. waterlogged', 'B. waterproof', 'C. water soaked', 'D. waterfall', 'a', 'Incomplete Sentence', 1, '2019-02-05 03:44:30'),
(4, 'reading', 'The train for London at platform 6 is still there and is not ....... to leave for at least 20 minutes.', '-.png', '-', 'A. intended', 'B. expected', 'C. proposed', 'awaited', 'c', 'Incomplete Sentence', 1, '2019-02-05 03:50:20'),
(5, 'reading', 'Unfortunately it is now 11 o\'clock and so there is a ....... of 15 minutes already.', '-.png', '-', 'A. waiting', 'B. lateness', 'C. space', 'D. delay', 'd', 'Incomplete Sentence', 1, '2019-02-05 03:51:19'),
(6, 'reading', 'Perhatikan kata yang digaris bawahi', '708267473.PNG', '-', 'A', 'B', 'C', 'D', 'c', 'Error Recognition', 1, '2019-02-05 03:55:43'),
(7, 'reading', 'Perhatikan kata yang digaris bawahi', '693765803.PNG', '-', 'A.', 'B.', 'C.', 'D.', 'b', 'Error Recognition', 1, '2019-02-05 04:02:39'),
(8, 'reading', 'Perhatikan kata yang digaris bawahi', '349171576.PNG', '-', 'A.', 'B.', 'C.', 'D.', 'a', 'Error Recognition', 1, '2019-02-05 04:04:32'),
(9, 'reading', 'Perhatikan kata yang digaris bawahi', '994996338.PNG', '-', 'A.', 'B.', 'C.', 'D.', 'a', 'Error Recognition', 1, '2019-02-05 04:07:06'),
(10, 'reading', 'Perhatikan kata yang digaris bawahi', '309651646.PNG', '-', 'A.', 'B.', 'C.', 'D.', 'b', 'Error Recognition', 1, '2019-02-05 04:09:07'),
(11, 'reading', 'It is pointed out in the reading that opera', '748162483.PNG', '-', 'A. has developed under the influence of musical theater.', 'B. is a drama sung with the accompaniment of an orchestra.', 'C. is not a high-budget production.', 'D. is often performed in Europe.', 'b', 'Comprehension', 1, '2019-02-05 11:39:02'),
(12, 'reading', 'We can understand from the reading that', '983406158.PNG', '-', 'A. people are captivated more by opera than musical theater.', 'B. drama in opera is more important than the music.', 'C. orchestras in operas can vary considerably in size.', 'D. musical theater relies above all on music.', 'c', 'Comprehension', 1, '2019-02-05 11:40:26'),
(13, 'reading', 'Which of the following processes led to the obliteration of the craters formed by the bombardment of the Earth by the celestial bodies?', '1000415754.PNG', '-', 'A. Volcanic activity', 'B. Solar radiation', 'C. Gravity activity', 'D. Crustal motions', 'd', 'Comprehension', 1, '2019-02-05 11:33:59'),
(14, 'reading', 'According to this passage, how do scientists estimate the age of the Earth?', '297824880.PNG', '-', 'A. By measuring the ratios of radioactive elements in rocks', 'B. By examining fossils', 'C. By studying sunspots', 'D. By examining volcanic activity', 'a', 'Comprehension', 1, '2019-02-05 11:34:12'),
(15, 'reading', 'According to the passage, why are scientists forced to look at other bodies in the solar system to determine the early history of the Earth?', '238061149.PNG', '-', 'A. Human alteration of the Earth', 'B. Erosion and crustal motions', 'C. Solar flares', 'D. Deforestation and global warming', 'b', 'Comprehension', 1, '2019-02-05 11:34:40'),
(16, 'listening', 'Who is most likely listening to the report?', '-.png', '78848840.mp3', 'A. Subway passengers', 'B. Vehicle driver', 'C. Business executives', 'D. Housewives', 'b', 'Short Talks', 1, '2019-02-05 05:35:54'),
(17, 'listening', 'What does the speaker say about traffic', '-.png', '49069060.mp3', 'A. It is flowing smoothly everywhere', 'B. It is clear on Interstate 7', 'C. It is show only on east-west roads', 'D. It is moving slowly everywhere', 'd', 'Short Talks', 1, '2019-02-05 05:39:14'),
(18, 'listening', 'What is scheduled to happen at 5:30 p.m.?', '-.png', '491135942.mp3', 'A. Clear roads', 'B. Main Street accident', 'C. A new traffic update', 'D. A baseball game', 'c', 'Short Talks', 1, '2019-02-05 05:41:39'),
(19, 'listening', 'Who is the message for?', '-.png', '708286065.mp3', 'A. Tanya Smith', 'B. Premium Insurance Company', 'C. Karl Klaussen', 'D. Toyaki Turbo', 'a', 'Short Talks', 1, '2019-02-05 05:43:11'),
(20, 'listening', 'What is the main purpose of the message ?', '-.png', '109100416.mp3', 'A. To issue a reminder', 'B. To provide information', 'C. To make an appointment', 'D. To note phone numbers', 'b', 'Short Talks', 1, '2019-02-05 05:44:18'),
(21, 'listening', 'What does the man plan to do?', '-.png', '873272411.mp3', 'A. Have a party', 'B. Buy office supplies', 'C. Take a vacation', 'D. Ask the woman out', 'a', 'Short Conversation', 1, '2019-02-05 05:46:32'),
(22, 'listening', 'Why does the man call the woman?', '-.png', '986489519.mp3', 'A. To inquire about prices', 'B. To sell her food', 'C. To invite her to a party', 'D. To order food and drinks', 'd', 'Short Conversation', 1, '2019-02-05 12:48:31'),
(23, 'listening', 'Where will the party be held?', '-.png', '239003960.mp3', 'A. In a city park', 'B. At he man s company', 'C. On the beach', 'D. In a theater', 'b', 'Short Conversation', 1, '2019-02-05 12:52:52'),
(24, 'listening', 'What are the speakers talking about?', '-.png', '451966554.mp3', 'A. The man s job', 'B. The man s salary', 'C. The woman s business', 'D. The woman s clients', 'c', 'Short Conversation', 1, '2019-02-05 12:52:30'),
(25, 'reading', 'How many clients does the woman have?', '-.png', '977107993.mp3', 'A. Two', 'B. Three', 'C. Four', 'D. Five', 'd', 'Short Conversation', 1, '2019-02-05 05:56:58'),
(26, 'reading', 'check', '-.png', '-', 'a.hghjhb', 'b.jhvbkjb', 'c.njknmn', 'd.hbjn', 'a', 'check', 1, '2019-02-05 18:44:03'),
(27, 'reading', 'This is a general notice for all passengers: the restaurant in the main ....... is now open.', '-.png', '-', 'A. concord', 'B. conduit', 'C. concourse', 'D. condition', 'c', 'Perhatikan kutipan tersebut !', 2, '2019-02-11 07:18:56'),
(28, 'reading', 'The small cafe situated near platform 6 is now ....... free cups of tea to those waiting for the London train.', '-.png', '-', 'A. discharging', 'B. dispensing', 'C. offering', 'D. disposing', 'c', 'Perhatikan kutipan tersebut !', 2, '2019-02-11 07:23:55');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tester`
--

CREATE TABLE `tbl_tester` (
  `id_tester` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user` int(10) NOT NULL,
  `id_paket` int(11) NOT NULL,
  `nilai` int(11) NOT NULL,
  `waktu_mengerjakan` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jawaban` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_tester`
--

INSERT INTO `tbl_tester` (`id_tester`, `id_user`, `id_paket`, `nilai`, `waktu_mengerjakan`, `jawaban`) VALUES
('2019020527', 2, 1, 100, '60', 'asdf'),
('2019020633', 6, 1, 0, '3428', '{\"1\":\"a\",\"2\":\"a\",\"3\":\"b\",\"4\":\"a\"}'),
('2019020647', 3, 1, 15, '2788', '{\"1\":\"b\",\"2\":\"d\",\"3\":\"b\",\"4\":\"b\",\"5\":\"b\",\"6\":\"c\",\"7\":\"c\",\"8\":\"d\",\"9\":\"b\",\"10\":\"c\",\"11\":\"c\",\"12\":\"b\",\"13\":\"c\",\"14\":\"c\",\"15\":\"c\",\"16\":\"c\",\"17\":\"c\",\"18\":\"c\",\"19\":\"d\",\"20\":\"a\",\"21\":\"c\",\"22\":\"d\",\"23\":\"b\",\"24\":\"d\",\"25\":\"a\",\"26\":\"b\"}'),
('2019020670', 4, 1, 0, '3577', '{\"11\":\"b\"}'),
('2019020671', 5, 1, 0, '3298', '{\"1\":\"a\",\"2\":\"a\",\"3\":\"b\"}'),
('2019020846', 8, 1, 0, '3305', '\"_token=JW23WDx0yh7800bvveLVobOdvaqmNtHEMdkF4rEl\"'),
('2019020873', 7, 1, 19, '3478', '\"_token=qAFO7RW4QXIDefCUnRr4fz7qfl6YXTcOYE04N1gp\"'),
('2019021188', 7, 2, 50, '3574', '{\"27\":\"c\",\"28\":\"a\"}');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isadmin` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nama`, `username`, `password`, `isadmin`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', '$2y$10$b94efByH2qKwc9y8YJdgqe7.KiU./h2kws0.FiTfKfuSwpzri5NXC', 1, 'pisHEHSuQymokFohxpkxDU3kSlhvWAXopcBF0gbUOexKFjCgpmQClH2rkBtA', '2019-02-05 03:27:22', NULL),
(2, 'Alfonso', 'alfi', '$2y$10$5S88ioWxsffUJXF3rfpjpuUT3NDUiWPxhJHqyrSI0En5vsrDsj62y', 0, 'ldgq3PkBClbV6kPfrHj6mEYCVf38AQ8aoL8CapTApLWUXGmc8HyE4cAnsWBL', '2019-02-05 03:27:22', '2019-02-05 19:50:15'),
(3, 'agyjon', 'agyjon', '$2y$10$XlkmawwCzx9reGX.wM5gAeWyFkyb8ObNbenshvWF5l6H0cE720JF6', 0, 'EP3AUyyTW7mUDE1M3uCtpuRIOOkTkV6RNtW5dxS983nM85aHzV36cxvtPFoy', '2019-02-05 20:31:54', '2019-02-05 20:31:54'),
(4, 'alfison', 'alfison', '$2y$10$xD7mLDrKBd1AJBVW6FwN9e8dGhYOO41UfZ/wfu2GZnmYXqinLJ4L.', 0, 'LaKwcFEdTEkdWRkcltqcr4CsmgGe3p8y7bzHCMBQc2xPyNWVhuWtQMjruxJr', '2019-02-05 22:27:21', '2019-02-05 22:27:21'),
(5, 'azizah', 'azizah', '$2y$10$qqMyQ1awc2TCsB8YlmWFF.w10cR02JS2kUjRAZsN.l87Ua/OOFOKq', 0, '22yelSUCyK560IRiA15Ysrbt1ckvHAERBA0dedw7P4r0zxT3UfQNn7xGVyjK', '2019-02-05 23:26:31', '2019-02-05 23:26:31'),
(6, 'Alfinia', 'alfinia', '$2y$10$lZu.vY1rszWywhoA4ywhPuErvAzuRbXNdIejSUb9NuumjUq.WKa9S', 0, 'Z6o4xaFV35exv2lludnG5Q7eG8G2frzUgePLNSEXPDiTNB464t8u7FXcg07l', '2019-02-05 23:42:35', '2019-02-05 23:42:36'),
(7, 'Akri SN', 'akri', '$2y$10$0.t4NJ8Qw4eSP2P1iN4p..mNBcZuQuSaHD5nAAZwnKvF8q0xVJG6S', 0, 'EgPlwyK5OvJTu6fgpAaHvT9s9Qz6StCjjGP7MBk4neaANAqiumJrdFhEZm5F', '2019-02-08 06:37:05', '2019-02-08 06:37:05'),
(8, 'Aldi R', 'aldi', '$2y$10$ts4gDxqDYBoWzZH/VwhukOGZBlfk36s3wt03b.K3C2uLFVvHZSvNy', 0, NULL, '2019-02-08 07:40:58', '2019-02-08 07:40:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_komentar`
--
ALTER TABLE `tbl_komentar`
  ADD PRIMARY KEY (`id_komentar`);

--
-- Indexes for table `tbl_paket`
--
ALTER TABLE `tbl_paket`
  ADD PRIMARY KEY (`id_paket`);

--
-- Indexes for table `tbl_soal`
--
ALTER TABLE `tbl_soal`
  ADD PRIMARY KEY (`id_soal`);

--
-- Indexes for table `tbl_tester`
--
ALTER TABLE `tbl_tester`
  ADD PRIMARY KEY (`id_tester`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_paket` (`id_paket`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_komentar`
--
ALTER TABLE `tbl_komentar`
  MODIFY `id_komentar` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_paket`
--
ALTER TABLE `tbl_paket`
  MODIFY `id_paket` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_soal`
--
ALTER TABLE `tbl_soal`
  MODIFY `id_soal` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
