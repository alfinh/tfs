<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\Tester;
use App\Soal;
use App\User;
use App\Paket;
use Carbon\Carbon;
use Auth;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index(){
      return view('frontend/index');
    }

    public function pengertian(){
      return view('frontend/pengertian');
    }

    public function tips(){
      return view('frontend/tips');
    }

    public function syarat(){
      return view('frontend/syarat');
    }

    public function pemetaansoal(){
      return view('frontend/pemetaansoal');
    }

    public function perbedaan(){
      return view('frontend/perbedaan');
    }

    public function caradaftar(){
      return view('frontend/caradaftar');
    }

    public function help(){
      return view('frontend/help');
    }

    public function materireading(){
      return view('frontend/reading/incomplete');
    }

    public function materilistening(){
      return view('frontend/listening/photograph');
    }

    public function formtes(){
      return view('frontend/formtes');
    }

    public function loginuser(){

      return view('frontend\login');

    }

    public function registeruser(Request $request){
            $pass = $request->input('password');
            $pass2 = $request->input('konfirmasipassword');

            $validate = \Validator::make($request->all(), [
                    'nama' => 'required',
                    'username'=> 'required',
                    'password' => 'required'
                    ],

                    $after_save = [
                        'alert' => 'danger',
                        'icon' => 'attention',
                        'title' => 'kesalahan !',
                        'text-1' => 'Ini terjadi dalam menginput',
                        'text-2' => 'Silakan coba lagi !'
                    ]);

                if($validate->fails()){
                    return redirect()->back()->with('after_save', $after_save);
                }

                $after_save = [
                          'alert' => 'success',
                          'icon' => 'check',
                          'title' => 'Berhasil ! ',
                          'text-1' => 'Akun ',
                          'text-2' => 'Telah dibuat.'
                      ];

            $now = Carbon::now();

            $data = new User();
               $data->nama = $request->nama;
               $data->username = $request->username;
               $data->password = bcrypt($request->password);
               $data->isadmin = 0;
               $data->created_at = $now;
               $data->save();

            return redirect('/login');

    }

    public function dashboarduser()
    {

      return view('frontend\dashboarduser');

    }


    public function testsoaldua(){


      $data = DB::select("SELECT * FROM tbl_soal where id_paket =2");
      $data_fetch = DB::table('tbl_tester')->where([
        ['id_user','=',Auth::user()->id],
        ['id_paket','=',"2"]
        ])->get()[0];
      $paket = DB::select("SELECT * FROM tbl_paket where id_paket =2");
      foreach ($paket as $pilihan) {
      $waktu = $pilihan->waktu_pengerjaan;

      return view('frontend/testsoaldua',['data'=>$data, 'waktu'=>$waktu, 'data_tes' => $data_fetch]);

      }
    }
    public function type(){

      $auth = Auth::user()->id;
      $paket1 = DB::table('tbl_tester')->where('id_paket','=',1)->where('id_user','=',$auth)->first();
      $paket2 = DB::table('tbl_tester')->where('id_paket','=',2)->where('id_user','=',$auth)->first();
      $paketsatu = DB::table('tbl_tester')->where('id_paket','=',1)->where('id_user','=',$auth)->count();
      $paketdua = DB::table('tbl_tester')->where('id_paket','=',2)->where('id_user','=',$auth)->count();
      return view('frontend/type',['paket1'=>$paket1, 'paket2'=>$paket2, 'paketsatu'=>$paketsatu, 'paketdua'=>$paketdua]);

    }

    public function testsoalsatu(){


      $data = DB::select("SELECT * FROM tbl_soal where id_paket =1");
      $data_fetch = DB::table('tbl_tester')->where([
        ['id_user','=',Auth::user()->id],
        ['id_paket','=',"1"]
        ])->get()[0];
      $paket = DB::select("SELECT * FROM tbl_paket where id_paket =1");
      foreach ($paket as $pilihan) {
      $waktu = $pilihan->waktu_pengerjaan;

      return view('frontend/testsoalsatu',['data'=>$data, 'waktu'=>$waktu, 'data_tes' => $data_fetch]);
     }

    }

    public function check(){
      $auth = Auth::user()->id;
      $data = DB::table('tbl_tester')->where('id_user','=',$auth)->where('id_paket','=',1)->count();
      $id_paket =1;
      if($data == 0){
      $yearnow = Carbon::now()->format('Y');
      $monthnow = Carbon::now()->format('m');
      $daynow = Carbon::now()->format('d');
      $idrandom= rand(10,99);
      $waktu = '60 Menit';
      $idakhir = "$yearnow$monthnow$daynow$idrandom";
      DB::table('tbl_tester')->insert([
        ['id_tester'=>$idakhir, 'id_user'=>Auth::user()->id,'id_paket'=>$id_paket, 'nilai'=>0, 'waktu_mengerjakan'=>3600, 'jawaban'=>'']
      ]);

      return redirect('/testsoalsatu');
    }else{
      return redirect('/choosetype');
    }

    }


    public function check2(){
      $auth = Auth::user()->id;
      $data = DB::table('tbl_tester')->where('id_user','=',$auth)->where('id_paket','=',2)->count();
      $id_paket =2;
      if($data == 0){
      $yearnow = Carbon::now()->format('Y');
      $monthnow = Carbon::now()->format('m');
      $daynow = Carbon::now()->format('d');
      $idrandom= rand(10,99);
      $waktu = '60 Menit';
      $idakhir = "$yearnow$monthnow$daynow$idrandom";
      DB::table('tbl_tester')->insert([
        ['id_tester'=>$idakhir, 'id_user'=>Auth::user()->id,'id_paket'=>$id_paket, 'nilai'=>0, 'waktu_mengerjakan'=>3600, 'jawaban'=>'']
      ]);

      return redirect('/testsoaldua');
    }else{
      return redirect('/testsoaldua');
    }

    }

    public function updatewaktu(Request $request){

      $waktu = $request->input('waktu_mengerjakan');
      $jawaban = $request->input('jawaban');

      $auth = Auth::user()->id;
      $data = DB::table('tbl_tester')->where([
        ['id_user','=',$auth],
        ['id_paket','=',1]
        ]
      )->update([
        'waktu_mengerjakan'=>$waktu,
        'jawaban'=>json_encode($jawaban)
        ]);


    }

    public function updatewaktudua(Request $request){

      $waktu = $request->input('waktu_mengerjakan');
      $jawaban = $request->input('jawaban');

      $auth = Auth::user()->id;
      $data = DB::table('tbl_tester')->where([
        ['id_user','=',$auth],
        ['id_paket','=',2]
        ]
      )->update([
        'waktu_mengerjakan'=>$waktu,
        'jawaban'=>json_encode($jawaban)
        ]);


    }

}
