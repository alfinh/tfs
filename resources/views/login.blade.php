<!-- Created by Alfi-->

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>TOEIC</title>
  <link rel="apple-touch-icon" href="{{asset('assets')}}/images/apple-touch-icon.png">
  <link rel="shortcut icon" type="{{asset('assets')}}/image/ico" href="{{asset('assets')}}/images/favicon.ico" />
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ asset('assets') }}/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('assets') }}/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('assets') }}/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body{
    background-color: rgba(0, 0 , 0, 0.5);
  }
  </style>
</head>

<body background="{{ asset('assets') }}/images/background3.jpg " style=" background-size: cover, contain; background-attachment: fixed; background-repeat: no-repeat; ">

<div class="login-box" >
  <div class="login-logo">

  </div>
  <!-- /.login-logo -->
  <div class="login-box-body" style="background-color:white; font-family:Calibri; border-radius:1%; opacity:0.93;">
    <center><img src="{{ asset('assets') }}/images/loginlog.png" style="height:58px; width:180px;"></center><br>

    @include('templates.feedback')

    <form action="{{ url('login') }}" method="post" autocomplete="off">
      {{ csrf_field() }}

      <div class="form-group has-feedback">
        <label style="color:#767a85;">Username</label>
        <input type="text" name="username" class="form-control" placeholder="username" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <label style="color:#767a85;">Password</label>
        <input type="password" name="password" class="form-control" placeholder="Password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div>
            <label>
              Lupa Kata Sandi Anda?
            </label>
          </div>

          <br>
        </div>
        <!-- /.col -->
        <div class="col-sm-12">
          <button type="submit" style="background-color:#292D30; color:white; border-radius:3%;"  class="btn btn-default btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <div class="social-auth-links text-center">
      <p style="color:#585d6a;">- TFS - Test  Helper Application System -</p>
      <p style="color:#585d6a;"> Copyright &copy; 2018<a href="#"> Alfi & Aziz</a></p>
    </div>


  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="{{ asset('assets') }}/js/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('assets') }}/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="{{ asset('assets') }}/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
