<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="author" content="John Doe">
    <meta name="description" content="">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Title -->
    <title>Home</title>
    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="{{asset('assets')}}/images/apple-touch-icon.png">
    <link rel="shortcut icon" type="{{asset('assets')}}/image/ico" href="{{asset('assets')}}/images/favicon.ico" />
    <!-- Plugin-CSS -->
    <link rel="stylesheet" href="{{asset('assets')}}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/themify-icons.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/magnific-popup.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/animate.css">
    <!-- Main-Stylesheets -->
    <link rel="stylesheet" href="{{asset('assets')}}/css/normalize.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/style.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/responsive.css">
    <script src="{{asset('assets')}}/js/vendor/modernizr-2.8.3.min.js"></script>

    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body data-spy="scroll" data-target="#primary-menu">
    <div class="preloader">
       <div class="sk-folding-cube">
           <div class="sk-cube1 sk-cube"></div>
           <div class="sk-cube2 sk-cube"></div>
           <div class="sk-cube4 sk-cube"></div>
           <div class="sk-cube3 sk-cube"></div>
       </div>
   </div>

    <!--Mainmenu-area-->
    <div class="mainmenu-area" data-spy="affix" data-offset-top="100">
        <div class="container">
            <!--Logo-->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary-menu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand logo">

                    <h2 style="position:relative; bottom:2px;"><img src="{{asset('assets')}}/images/logo.png" width="55px" > TOEIC</h2>
                </a>
            </div>
            <!--Logo/-->
            <nav class="collapse navbar-collapse" id="primary-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="#home-page">Home</a></li>
                    <li><a href="{{url('/dashboarduser')}}">Materials & Test</a></li>
                    <li><a href="#feature-page">About TOEIC</a></li>
                    <li><a href="#team-page">Team</a></li>
                    <li><a href="#contact-page">Contact</a></li>
                    <li><a href="/help">Help</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <!--Mainmenu-area/-->



    <!--Header-area-->
    <header class="header-area overlay full-height relative v-center" id="home-page">
        <div class="absolute anlge-bg"></div>
        <div class="container">
            <div class="row v-center">
                <div class="col-xs-12 col-md-7 header-text">
                    <h2>It’s all about TOEIC for Success</h2>
                    <p>Web containing or discussing all general or specific knowledge about TOEIC. And you will be presented in full.
                      You can use this web to measure the ability of the TOEIC itself. We will achieve our dream work with satisfying Toeic values</p>
                    <a href="{{URL('/formtes')}}" class="button white"><i class="glyphicon glyphicon-education" style="font-size:17px; position: relative;top:4px; right:3px;"></i> Test Soal</a>
                </div>
                <div class="hidden-xs hidden-sm col-md-5 text-right">
                    <div class="screen-box screen-slider">
                        <div class="item">
                            <img src="{{asset('assets')}}/images/laptop.png" alt="">
                        </div>
                        <div class="item">
                            <img src="{{asset('assets')}}/images/headphone2.png" width="400px" height="500px" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </header>
    <!--Header-area/-->

    <section class="angle-bg sky-bg section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div id="caption_slide" class="carousel slide caption-slider" data-ride="carousel" style="font-color:white;">
                        <div class="carousel-inner" role="listbox">
                            <div class="item active row">
                                <div class="v-center">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="caption-title" data-animation="animated fadeInUp">
                                            <h2 style="color:white;">Easy to understand</h2>
                                        </div>
                                        <div class="caption-desc" data-animation="animated fadeInUp">
                                            <p style="color:white;">
                                              This application is designed to make it easier for users to learn and understand the material that exists and is able to absorb material. In order for users who will be able to run to the next part, namely the test questions and hopefully they can do it.</p>
                                        </div>
                                        <div class="caption-button" data-animation="animated fadeInUp">
                                            <a href="#" class="button">Read more</a>
                                        </div>
                                    </div>

                                    <div class="col-xs-6 col-md-3">
                                        <div class="caption-photo one" data-animation="animated fadeInRight">
                                            <img src="{{asset('assets')}}/images/screen-1.jpg" alt="">
                                        </div>
                                    </div>

                                    <div class="col-xs-6 col-md-3">
                                        <div class="caption-photo two" data-animation="animated fadeInRight">
                                            <img src="{{asset('assets')}}/images/screen-2.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item row">
                                <div class="v-center">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="caption-title" data-animation="animated fadeInUp">
                                            <h2 style="color:white;">Easy to use</h2>
                                        </div>
                                        <div class="caption-desc" data-animation="animated fadeInUp">
                                            <p style="color:white;">
                                              This application is easy to use because this application is made based on complaints and aspirations of the aspirations of people who will use this application. Hopefully this application can help and be used in the future. I hope this application can develop better for the future</p>
                                        </div>
                                        <div class="caption-button" data-animation="animated fadeInUp">
                                            <a href="#" class="button">Read more</a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <div class="caption-photo one" data-animation="animated fadeInRight">
                                            <img src="{{asset('assets')}}/images/screen-3.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <div class="caption-photo two" data-animation="animated fadeInRight">
                                            <img src="{{asset('assets')}}/images/screen-4.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item row">
                                <div class="v-center">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="caption-title" data-animation="animated fadeInUp">
                                            <h2>Easy to test</h2>
                                        </div>
                                        <div class="caption-desc" data-animation="animated fadeInUp">
                                            <p>this application is equipped with test questions with usage or form like other web browsers but in doing this the web has its own characteristics. That is easy to do, UI is simple and does not hurt the eyes and the absolute matter. The question test is expected to be able to test the readiness of someone who will later implement the actual TOEIC test (
                                              Test of English for International Communication)</p>
                                        </div>
                                        <div class="caption-button" data-animation="animated fadeInUp">
                                            <a href="#" class="button">Read more</a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <div class="caption-photo one" data-animation="animated fadeInRight">
                                            <img src="{{asset('assets')}}/images/screen-3.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <div class="caption-photo two" data-animation="animated fadeInRight">
                                            <img src="{{asset('assets')}}/images/screen-4.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Indicators -->
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section class="gray-bg section-padding" id="feature-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <div class="page-title">
                        <h2>ABOUT TOEIC</h2>
                        <p>Disini Lengkap Seputar TOEIC.</p>
                    </div>
                </div>
            </div>
            <div class="row">
              <a href="/pengertian"><div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="box">
                        <div class="box-icon">
                            <img src="{{asset('assets')}}/images/portfolio-icon-1.png" alt="">
                        </div>
                        <h3>PENGERTIAN UMUM</h3>
                        <p>Berisikan Pengertian umum secara bahasa atau harfiah</p>
                    </div>
                </div></a>
             <a href="/tips"><div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="box">
                        <div class="box-icon">
                            <img src="{{asset('assets')}}/images/portfolio-icon-1.png" alt="">
                        </div>
                        <h3>TIPS</h3>
                        <p>Berikan tips-tips yang harus anda lakukan untuk menghadapi TOEIC</p>
                    </div>
                </div>
              <a href="/syarat"><div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="box">
                        <div class="box-icon">
                            <img src="{{asset('assets')}}/images/portfolio-icon-3.png" alt="">
                        </div>
                        <h3>SYARAT</h3>
                        <p>Berisikan syarat-syarat yang harus anda penuhi nantinya sebelum melakukan TOEIC</p>
                    </div>
                </div>
            <a href="/pemetaansoal"><div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="box">
                        <div class="box-icon">
                            <img src="{{asset('assets')}}/images/portfolio-icon-4.png" alt="">
                        </div>
                        <h3>PEMETAAN SOAL</h3>
                        <p>Berisikan Klasifikasi dan penjelasan tentang apa dan bagimana nanti soal sesungguhnya</p>
                    </div>
                </div></a>
            <a href="/caradaftar"><div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="box">
                        <div class="box-icon">
                            <img src="{{asset('assets')}}/images/portfolio-icon-5.png" alt="">
                        </div>
                        <h3>Cara Daftar</h3>
                        <p>Untuk melakukan Test anda harus tahu dulu cara daftarnya</p>
                    </div>
                </div></a>
            <a href="/perbedaan"><div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="box">
                        <div class="box-icon">
                            <img src="{{asset('assets')}}/images/portfolio-icon-6.png" alt="">
                        </div>
                        <h3>Perbedaan TOEIC dan TOEFL</h3>
                        <p>Terkadang anda kebingungan jadi Apa perbedaan TOEIC dan TOEFL , disini penjelasanya</p>
                    </div>
                </div></a>
            </div>
        </div>
    </section>






    <section class="section-padding gray-bg" id="team-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <div class="page-title">
                        <h2>Our team</h2>
                        <p>Disini Orang-orang yang memiliki 1 ide yang menarik dan menimplementasikanya lewat aplikasi ini</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3">

                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="single-team">
                        <div class="team-photo">
                            <img src="{{asset('assets')}}/images/alfi.jpg" alt="">
                        </div>
                        <h4>Alfi Nur Hakim</h4>
                        <h6>Web Programmer</h6>
                        <ul class="social-menu">
                            <li><a href="#"><i class="ti-facebook"></i></a></li>
                            <li><a href="#"><i class="ti-twitter"></i></a></li>
                            <li><a href="#"><i class="ti-linkedin"></i></a></li>
                            <li><a href="#"><i class="ti-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="single-team">
                        <div class="team-photo">
                            <img src="{{asset('assets')}}/images/aziz.jpg" alt="">
                        </div>
                        <h4>M Abdul Aziz</h4>
                        <h6>Web Designer</h6>
                        <ul class="social-menu">
                            <li><a href="#"><i class="ti-facebook"></i></a></li>
                            <li><a href="#"><i class="ti-twitter"></i></a></li>
                            <li><a href="#"><i class="ti-linkedin"></i></a></li>
                            <li><a href="#"><i class="ti-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
            </div>
        </div>
    </section>




    <section class="testimonial-area section-padding gray-bg overlay">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <div class="page-title">
                        <h2>Quote Of Succes</h2>
                        <p>Dibawah ini 3 kata kata indah dari orang orang sukses</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                    <div class="testimonials">
                        <div class="testimonial">
                            <div class="testimonial-photo">
                                <img src="{{asset('assets')}}/images/bob_sadino.jpg" alt="">
                            </div>
                            <h3>Bob Sadino</h3>
                            <p>“Sukses bukanlah sebuah kebetulan, ia adalah hasil kerja keras. Pemenang bukan mereka yang tak pernah gagal, tapi mereka yang tidak pernah menyerah”</p>
                        </div>
                        <div class="testimonial">
                            <div class="testimonial-photo">
                                <img src="{{asset('assets')}}/images/bill_gates.jpg" alt="">
                            </div>
                            <h3>Bill Gates</h3>
                            <p>“Aku tidak akan menyerah, akan ku jadikan kegagalan dan penolakan sebagai langkah pertama untuk mencapai keberhasilan”</p>
                        </div>
                        <div class="testimonial">
                            <div class="testimonial-photo">
                                <img src="{{asset('assets')}}/images/jack-ma.jpg" alt="">
                            </div>
                            <h3>Jack Ma</h3>
                            <p>“Banyak orang yang tahu apa yang harus dilakukan, tetapi hanya sedikit orang yang benar-benar melakukan apa yang mereka ketahui”</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <footer class="footer-area relative sky-bg" id="contact-page">
        <div class="absolute footer-bg"></div>
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                        <div class="page-title">
                            <h2>Contact US</h2>
                            <p>Anda bisa menghubungi kami lewat keterangan  dibawah</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <address class="side-icon-boxes">
                            <div class="side-icon-box">
                                <div class="side-icon">
                                    <img src="{{asset('assets')}}/images/location-arrow.png" alt="">
                                </div>
                                <p><strong>Address: </strong> Jl. Kliningan No.6, Turangga, Lengkong, Kota Bandung</p>
                            </div>
                            <div class="side-icon-box">
                                <div class="side-icon">
                                    <img src="{{asset('assets')}}/images/phone-arrow.png" alt="">
                                </div>
                                <p><strong>Telephone: </strong>
                                    <a href="callto:8801812726495">+8801812726495</a> <br />
                                    <a href="callto:8801687420471">+8801687420471</a>
                                </p>
                            </div>
                            <div class="side-icon-box">
                                <div class="side-icon">
                                    <img src="{{asset('assets')}}/images/mail-arrow.png" alt="">
                                </div>
                                <p><strong>E-mail: </strong>
                                    <a href="#">alfinurhakim18@example.com</a> <br />
                                    <a href="#">aziz1kenshin@mail.com</a>
                                </p>
                            </div>
                        </address>
                    </div>
                    <div class="col-xs-12 col-md-8">
                      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.577519403545!2d107.62688511423539!3d-6.9409880949851805!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e86413eb50ad%3A0xf9930b5268e3fee1!2sSekolah+Menengah+Kejuruan+4+Bandung!5e0!3m2!1sid!2sid!4v1540106021751"
                      width="770px" height="400px;" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-middle">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 pull-right">
                        <ul class="social-menu text-right x-left">
                            <li><a href="#"><i class="ti-facebook"></i></a></li>
                            <li><a href="#"><i class="ti-twitter"></i></a></li>
                            <li><a href="#"><i class="ti-google"></i></a></li>
                            <li><a href="#"><i class="ti-linkedin"></i></a></li>
                            <li><a href="#"><i class="ti-github"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <p>Anda bisa menghubungi kami lebih lanjut untuk menanyakan hal hal yang penting demi kesempurnaan web ini berjalan .</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <p>&copy;Copyright 2018 All right resurved. by Alfi & Aziz</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>





    <!--Vendor-JS-->
    <script src="{{asset('assets')}}/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="{{asset('assets')}}/js/vendor/bootstrap.min.js"></script>
    <!--Plugin-JS-->
    <script src="{{asset('assets')}}/js/owl.carousel.min.js"></script>
    <script src="{{asset('assets')}}/js/contact-form.js"></script>
    <script src="{{asset('assets')}}/js/jquery.parallax-1.1.3.js"></script>
    <script src="{{asset('assets')}}/js/scrollUp.min.js"></script>
    <script src="{{asset('assets')}}/js/magnific-popup.min.js"></script>
    <script src="{{asset('assets')}}/js/wow.min.js"></script>
    <!--Main-active-JS-->
    <script src="{{asset('assets')}}/js/main.js"></script>
</body>

</html>
