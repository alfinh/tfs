<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\Soal;
use Carbon\Carbon;
use Auth;
use DataTables;
use Illuminate\Http\Request;

class SoalController extends Controller
{

  public function paketsatu(){

    $jumlahall = DB::table('tbl_soal')->where('id_paket','=',1)->count();
    $jumlahreading = DB::table('tbl_soal')->where('id_paket','=',1)->where('tipe_soal','=','reading')->count();
    $jumlahlistening = DB::table('tbl_soal')->where('id_paket','=',1)->where('tipe_soal','=','listening')->count();
    return view('backend\paket1\soalsatu',['jumlahall'=>$jumlahall , 'jumlahreading'=>$jumlahreading, 'jumlahlistening'=>$jumlahlistening]);

  }

  public function jsonsatu(Request $request){

    if($request->get('tgl_mulai') && $request->get('tgl_akhir'))
     {
         $tgl_mulai  = $request->get('tgl_mulai');
         $tgl_akhir    = $request->get('tgl_akhir');

          $soalsatu = DB::table('tbl_soal')->where('id_paket','=',1)->whereBetween('waktu_pembuatan',[$tgl_mulai,$tgl_akhir])->get();
          return Datatables::of($soalsatu)
          ->addColumn('action', function($soalsatu){
          return '<a href="#" data-toggle="modal" data-target="#editForm" class="btn btn-xs btn-default edit" id="editclick" ><i class="glyphicon glyphicon-edit"></i></a>
                  <a href="delete_soalsatu/'.$soalsatu->id_soal.'" class="btn btn-xs btn-danger delete" id=""><i class="glyphicon glyphicon-remove"></i></a>';

    })

    ->make(true);
    }else{
            $soalsatu = DB::table('tbl_soal')->where('id_paket','=',1)->get();
            return Datatables::of($soalsatu)
            ->addColumn('action', function($soalsatu){
            return '<a href="#" data-toggle="modal" data-target="#editForm" class="btn btn-xs btn-default edit" id="editclick"><i class="glyphicon glyphicon-edit"></i></a>
                    <a href="delete_soalsatu/'.$soalsatu->id_soal.'" class="btn btn-xs btn-danger delete" id=""><i class="glyphicon glyphicon-remove"></i></a>';

        })

        ->make(true);
    }

  }

  public function paketdua(){

    $jumlahall = DB::table('tbl_soal')->where('id_paket','=',2)->count();
    $jumlahreading = DB::table('tbl_soal')->where('id_paket','=',2)->where('tipe_soal','=','reading')->count();
    $jumlahlistening = DB::table('tbl_soal')->where('id_paket','=',2)->where('tipe_soal','=','listening')->count();
    return view('backend\paket2\soaldua',['jumlahall'=>$jumlahall , 'jumlahreading'=>$jumlahreading, 'jumlahlistening'=>$jumlahlistening]);

  }

  public function jsondua(Request $request){

    if($request->get('tgl_mulai') && $request->get('tgl_akhir'))
     {
         $tgl_mulai  = $request->get('tgl_mulai');
         $tgl_akhir    = $request->get('tgl_akhir');

          $soaldua = DB::table('tbl_soal')->where('id_paket','=',2)->whereBetween('waktu_pembuatan',[$tgl_mulai,$tgl_akhir])->get();
          return Datatables::of($soaldua)
          ->addColumn('action', function($soaldua){
          return '<a href="#" data-toggle="modal" data-target="#editForm" class="btn btn-xs btn-default edit" id="editclick" ><i class="glyphicon glyphicon-edit"></i></a>
                  <a href="delete_soaldua/'.$soaldua->id_soal.'" class="btn btn-xs btn-danger delete" id=""><i class="glyphicon glyphicon-remove"></i></a>';

    })

    ->make(true);
    }else{
            $soaldua = DB::table('tbl_soal')->where('id_paket','=',2)->get();
            return Datatables::of($soaldua)
            ->addColumn('action', function($soaldua){
            return '<a href="#" data-toggle="modal" data-target="#editForm" class="btn btn-xs btn-default edit" id="editclick"><i class="glyphicon glyphicon-edit"></i></a>
                    <a href="delete_soaldua/'.$soaldua->id_soal.'" class="btn btn-xs btn-danger delete" id=""><i class="glyphicon glyphicon-remove"></i></a>';

        })

        ->make(true);
    }

  }

  public function store_soal(Request $request){
     $after_save = [
               'alert' => 'success',
               'icon' => 'check',
               'title' => 'Berhasil ! ',
               'text-1' => 'Data soal ',
               'text-2' => 'Telah ditambah.'
           ];

           $now =  Carbon::now();

           $data = new Soal();
              $data->tipe_soal = $request->input('tipe_soal');
              $data->soal_tulisan = $request->input('soal_tulisan');
              $data->jawaban_a = $request->input('jawaban_a');
              $data->jawaban_b = $request->input('jawaban_b');
              $data->jawaban_c = $request->input('jawaban_c');
              $data->jawaban_d = $request->input('jawaban_d');
              $data->jawaban_asli = $request->input('jawaban_asli');
              $data->keterangan = $request->input('keterangan');
              $data->id_paket = $request->input('paket');
              $data->waktu_pembuatan = $now;
              $file = $request->file('soal_gambar');
              if($file == null){
                $data->soal_gambar = '-.png';
              }else{
              $ext = $file->getClientOriginalExtension();
              $newName = rand(100000,1001238912).".".$ext;
              $file->move('uploads/file',$newName);
              $data->soal_gambar = $newName;
              }
              $suara = $request->file('soal_suara');
              if($suara == null){
                $data->soal_suara= '-';
              }else{
              $extsuara = $suara->getClientOriginalExtension();
              $newNamesuara = rand(100000,1001238912).".".$extsuara;
              $suara->move('uploads/file',$newNamesuara);
              $data->soal_suara= $newNamesuara;
              }
              $data->save();

           if($request->input('paket') == 1){

           return redirect('/paketsatu')->with('after_save', $after_save);

           }else{

           return redirect('/paketdua')->with('after_save', $after_save);

           }
   }

   public function destroy_soalsatu($id) {
                DB::delete('delete from tbl_soal where id_soal = ?',[$id]);
                $after_save = [
                          'alert' => 'success',
                          'icon' => 'check',
                          'title' => 'Berhasil ! ',
                          'text-1' => 'Data soal ',
                          'text-2' => 'Telah dihapus.'
                      ];

                return redirect('/paketsatu')->with('after_save', $after_save);
     }

     public function destroy_soaldua($id) {
                  DB::delete('delete from tbl_soal where id_soal = ?',[$id]);
                  $after_save = [
                            'alert' => 'success',
                            'icon' => 'check',
                            'title' => 'Berhasil ! ',
                            'text-1' => 'Data soal ',
                            'text-2' => 'Telah dihapus.'
                        ];

                  return redirect('/paketdua')->with('after_save', $after_save);
       }

       public function update_soal(Request $request){
         $id= $request->input('id_soal');
         $after_save = [
                 'alert' => 'success',
                 'icon' => 'check',
                 'title' => 'Berhasil ! ',
                 'text-1' => 'Data Soal ',
                 'text-2' => 'telah diubah.'
             ];


             $data = Soal::findOrFail($id);
                 $data->tipe_soal = $request->input('tipe_soal');
                 $data->soal_tulisan = $request->input('soal_tulisan');
                 $data->jawaban_a = $request->input('jawaban_a');
                 $data->jawaban_b = $request->input('jawaban_b');
                 $data->jawaban_c = $request->input('jawaban_c');
                 $data->jawaban_d = $request->input('jawaban_d');
                 $data->jawaban_asli = $request->input('jawaban_asli');
                 $data->keterangan = $request->input('keterangan');
                 $data->waktu_pembuatan;

                 if (empty($request->file('soal_gambar'))){
                     $data->soal_gambar= $data->soal_gambar;
                 }
                 else{
                    if($data->soal_gambar == '-.png'){

                      $file = $request->file('soal_gambar');
                      $ext = $file->getClientOriginalExtension();
                      $newName = rand(100000,1001238912).".".$ext;
                      $file->move('uploads/file',$newName);
                      $data->soal_gambar = $newName;

                    }else{

                     unlink('uploads/file/'.$data->soal_gambar); //menghapus file lama
                     $file = $request->file('soal_gambar');
                     $ext = $file->getClientOriginalExtension();
                     $newName = rand(100000,1001238912).".".$ext;
                     $file->move('uploads/file',$newName);
                     $data->soal_gambar = $newName;

                   }
                 }

                 if (empty($request->file('soal_suara'))){
                     $data->soal_suara= $data->soal_suara;
                 }
                 else{
                     unlink('uploads/file/'.$data->soal_suara); //menghapus file lama
                     $file = $request->file('soal_suara');
                     $ext = $file->getClientOriginalExtension();
                     $newName = rand(100000,1001238912).".".$ext;
                     $file->move('uploads/file',$newName);
                     $data->soal_suara = $newName;
                 }

                 $data->save();

                 if($request->input('paket') == 1){

                 return redirect('/paketsatu')->with('after_save', $after_save);

                 }else{

                 return redirect('/paketdua')->with('after_save', $after_save);

                 }


       }
}
