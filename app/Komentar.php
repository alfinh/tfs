<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
  protected $table='tbl_komentar';

  protected $primaryKey = 'id_komentar';


  protected $fillable = [
    'id_komentar','nm_pengirim','komentar','ket_waktu'
  ];
}
