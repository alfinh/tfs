<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTester extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('tbl_tester', function (Blueprint $table) {
          $table->string('id_tester',20)->primary();
          $table->integer('id_user')->length(10);
          $table->integer('id_paket')->length(10);
          $table->integer('nilai')->length(11);
          $table->string('waktu_mengerjakan',15);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_tester');
    }
}
