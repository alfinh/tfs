@extends('templates/user')

@section('content')
<div class="container"><br><br><br><br>
  {{-- part alert --}}
         @if (Session::has('after_save'))
             <div class="col-md-12">
                 <div class="alert alert-dismissible alert-{{ Session::get('after_save.alert') }}">
                   <i class="pe-7s-{{ Session::get('after_save.icon') }}" style="font-size:30px; position:relative; top:8px;"></i>
                   <button type="button" class="close" data-dismiss="alert">×</button>
                   <strong>{{ Session::get('after_save.title') }}</strong>
                   <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_save.text-1') }}</a> {{ Session::get('after_save.text-2') }}
                 </div>
             </div>
         @endif
   {{-- end part alert --}}
      <div class="row" style="position:relative; top:20px;">
        @if($paketsatu == 1)
          @if($paket1->nilai == 0 )
              <a href="#" data-toggle="modal" data-target="#lanjut">
          @else
              <a href="/history">
          @endif
        @else
            <a href="#" data-toggle="modal" data-target="#paketeasy">
        @endif
              <div class="col-xs-12 col-sm-4 col-sm-offset-2">
                <div class="box">
                  <div class="box-icon">
                      <img src="{{asset('assets')}}/images/portfolio-icon-1.png" alt="">
                  </div>
                  <h2>PAKET 1 (EASY)</h2>
                  <p>Tingkat kesulitan test dipaket ini masih mudah dan gampang untuk diisi</p>
        @if($paketsatu == 0)
                  <button type="button" class="btn btn-md btn-fill btn-primary"> <i class="glyphicon glyphicon-bookmark"></i> Kerjakan Test</button>
        @else
                  <a href="/history" class="btn btn-md btn-fill btn-primary"> <i class="glyphicon glyphicon-bookmark"></i> Check History</a>
        @endif
               </div>
              </div>
            </a>

            <div class="modal fade" tabindex="-1" id="paketeasy" role="dialog" data-backdrop="false">
                   <div class="modal-dialog" role="document" >
                       <div class="modal-content">
                           <!-- Modal Header -->
                           <div class="modal-header">
                             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                               <span aria-hidden="true"> &times;</span>
                             </button>
                             <h3 class="modal-title"><b>Ketentuan & Informasi Paket 1 </b></h3>
                           </div>
                             <div class="modal-body">
                                <p><b><i>Paket 1</i></b> merupakan paket dengan tingkat kesulitan rendah dan mudah saat mengerjakan bagi user atau tester.
                                         Diharapkan user mengerjakanya dengan serius untuk mengukur kemampuan seberapa jauh kah pemahaman TOEIC tentang TOEIC.</p>
                                <p><b>Jumlah Soal</b> : 30 Soal</p>
                                <p><b>Soal Reading</b> : 15 Soal</p>
                                <p><b>Soal Listening</b> : 15 Soal</p>
                                <p><b>Waktu Pengerjaan</b> : 60 Menit</p>
                                Jika anda mengerjakan lebih dari waktu yang ditentukan , Anda akan disajikan langsung nilai sesuai yang anda isi.
                                Jadi , Patuhi ketentuan dan pahami informasi soalnya.<br><br>
                                <h4><i>Selamat Mengerjakan.</i></h4>
                            </div>

                          <div class="modal-footer">
                              <button type="button" class="btn btn-raised btn-default btn-fill"  data-dismiss="modal" aria-label="Close"> Cancel <i class="glyphicon glyphicon-arrow-left"> </i></button>
                              <a href="{{URL('/checkedtest')}}" class="btn btn-sm btn-fill btn-primary"><i class="glyphicon glyphicon-bookmark"></i> Kerjakan</a>
                          </div>
                  </div>
              </div>
            </div>


            <div class="col-xs-12 col-sm-4 ">
                <div class="box">
                  @if($paketdua == 1)
                    @if($paket2->nilai == 0 )
                        <a href="#" data-toggle="modal" data-target="#lanjut">
                    @else
                        <a href="/history">
                    @endif
                  @else
                      <a href="#" data-toggle="modal" data-target="#pakethard">
                  @endif
                    <div class="box-icon">
                        <img src="{{asset('assets')}}/images/portfolio-icon-6.png" alt="">
                    </div>
                    <h2>PAKET 2 (HARD)</h2>
                  <p>Tingkat kesulitan test dipaket ini tinggi dan tester sering kali sukar</p>
                  @if($paketdua == 0)
                            <button type="button" class="btn btn-md btn-fill btn-primary"> <i class="glyphicon glyphicon-bookmark"></i> Kerjakan Test</button>
                  @else
                            <a href="/history" class="btn btn-md btn-fill btn-primary"> <i class="glyphicon glyphicon-bookmark"></i> Check History</a>
                  @endif
                </a>
                  </div>

                  <div class="modal fade" tabindex="-1" id="pakethard" role="dialog" data-backdrop="false">
                         <div class="modal-dialog" role="document" >
                             <div class="modal-content">
                                 <!-- Modal Header -->
                                 <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                     <span aria-hidden="true"> &times;</span>
                                   </button>
                                   <h3 class="modal-title"><b>Ketentuan & Informasi Paket 2 </b></h3>
                                 </div>
                                   <div class="modal-body">
                                      <p><b><i>Paket 2</i></b> merupakan paket dengan tingkat kesulitan tinggi dan sukar saat mengerjakan bagi user atau tester.
                                               Diharapkan user mengerjakanya dengan serius untuk mengukur kemampuan seberapa jauh kah pemahaman TOEIC tentang TOEIC.</p>
                                      <p><b>Jumlah Soal</b> : 30 Soal</p>
                                      <p><b>Soal Reading</b> : 15 Soal</p>
                                      <p><b>Soal Listening</b> : 15 Soal</p>
                                      <p><b>Waktu Pengerjaan</b> : 90 Menit</p>
                                      Jika anda mengerjakan lebih dari waktu yang ditentukan , Anda akan disajikan langsung nilai sesuai yang anda isi.
                                      Jadi , Patuhi ketentuan dan pahami informasi soalnya.<br><br>
                                      <h4><i>Selamat Mengerjakan.</i></h4>
                                  </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-raised btn-default btn-fill"  data-dismiss="modal" aria-label="Close"> Cancel <i class="glyphicon glyphicon-arrow-left"> </i></button>
                                    <a href="{{URL('/checkedtest2')}}"  class="btn btn-sm btn-fill btn-primary"><i class="glyphicon glyphicon-bookmark"></i> Kerjakan</a>
                                </div>
                        </div>
                    </div>
                  </div>

                  <div class="modal fade" tabindex="-1" id="lanjut" role="dialog" data-backdrop="false">
                         <div class="modal-dialog" role="document" >
                             <div class="modal-content">
                                 <!-- Modal Header -->
                                 <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                     <span aria-hidden="true"> &times;</span>
                                   </button>
                                   <h3 class="modal-title"><b>Status Pengerjaan Test</b></h3>
                                 </div>
                                   <div class="modal-body">
                                     <p><i>Ada test yang belum anda bereskan, anda harus menyelesaikan terlebih dahulu.</i></p>
                                      <h1><i>Lanjut Mengerjakan !</i></h1><br>
                                  </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-raised btn-default btn-fill"  data-dismiss="modal" aria-label="Close"> Cancel <i class="glyphicon glyphicon-arrow-left"> </i></button>
                                  <a href="{{URL('/checkedtest')}}" class="btn btn-sm btn-fill btn-primary"><i class="glyphicon glyphicon-bookmark"></i> Lanjut Kerjakan</a>
                                </div>
                        </div>
                    </div>
                  </div>

                  <div class="modal fade" tabindex="-1" id="lanjut2" role="dialog" data-backdrop="false">
                         <div class="modal-dialog" role="document" >
                             <div class="modal-content">
                                 <!-- Modal Header -->
                                 <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                     <span aria-hidden="true"> &times;</span>
                                   </button>
                                   <h3 class="modal-title"><b>Status Pengerjaan Test</b></h3>
                                 </div>
                                   <div class="modal-body">
                                     <p><i>Ada test yang belum anda bereskan, anda harus menyelesaikan terlebih dahulu.</i></p>
                                      <h1><i>Lanjut Mengerjakan !</i></h1><br>
                                  </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-raised btn-default btn-fill"  data-dismiss="modal" aria-label="Close"> Cancel <i class="glyphicon glyphicon-arrow-left"> </i></button>
                                  <a href="{{URL('/checkedtest2')}}" class="btn btn-sm btn-fill btn-primary"><i class="glyphicon glyphicon-bookmark"></i> Lanjut Kerjakan</a>
                                </div>
                        </div>
                    </div>
                  </div>

            </div>

        </div>
    <br>
</div>

@endsection
