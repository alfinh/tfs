<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="author" content="John Doe">
    <meta name="description" content="">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Title -->
    <title>TOEIC</title>
    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="{{asset('assets')}}/images/apple-touch-icon.png">
    <link rel="shortcut icon" type="{{asset('assets')}}/image/ico" href="{{asset('assets')}}/images/favicon.ico" />
    <!-- Plugin-CSS -->
    @stack('css')
    <link rel="stylesheet" href="{{asset('assets')}}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/themify-icons.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/magnific-popup.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/animate.css">
    <!-- Main-Stylesheets -->
    	<link rel="stylesheet" href="{{asset('assets')}}/css/main.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/normalize.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/style2.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/responsive.css">
    <script src="{{asset('assets')}}/js/vendor/modernizr-2.8.3.min.js"></script>

    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body data-spy="scroll" data-target="#primary-menu" style="background-color:#FDFDFD;">

      <div class="preloader">
         <div class="sk-folding-cube">
             <div class="sk-cube1 sk-cube"></div>
             <div class="sk-cube2 sk-cube"></div>
             <div class="sk-cube4 sk-cube"></div>
             <div class="sk-cube3 sk-cube"></div>
         </div>
     </div>

    <!--Mainmenu-area-->
    <div class="top_header" id="home" >
  		<!-- Fixed navbar -->
  		<nav class="navbar navbar-default navbar-fixed-top" >
  			<div class="nav_top_fx_w3ls_agileinfo">
  				<div class="navbar-header">
  					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
  					    aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
  					<div class="logo-w3layouts-agileits" style="margin-top:5px;">
  						<h1> <a class="navbar-brand" href="#">TOEIC <span class="desc">Test English</span></a></h1>
  					</div>
  				</div>
  				<div id="navbar" class="navbar-collapse collapse">
  					<div class="nav_right_top">
  						<ul class="nav navbar-nav">
                <li class="active"><a href="/#home-page">Home</a></li>
                <li><a href="/#service-page">Materials & Test</a></li>
                <li><a href="/#feature-page">About TOEIC</a></li>
                <li><a href="/#team-page">Team</a></li>
                <li><a href="/#contact-page">Contact</a></li>
                <li><a href="/help">Help</a></li>
  						</ul>
  					</div>
  				</div>
  				<!--/.nav-collapse -->
  			</div>
  		</nav>
  	</div><br><br><br><br><br><br>
  	<!-- banner -->
    @yield('content')<br>
    <!--Feature-area/-->


    <footer class="footer-area relative sky-bg" id="contact-page">
        <div class="absolute footer-bg"></div>
          <div class="footer-bottom">
              <div class="container">
                  <div class="row">
                      <div class="col-xs-12 text-center">
                          <p>&copy;<span style="color:white;">Copyright 2018 All right resurved. by Alfi & Aziz</span></p>
                      </div>
                  </div>
              </div>
          </div>
    </footer>






    <!--Vendor-JS-->
    @stack('script')
    <script src="{{asset('assets')}}/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="{{asset('assets')}}/js/vendor/bootstrap.min.js"></script>
    <!--Plugin-JS-->
    <script src="{{asset('assets')}}/js/owl.carousel.min.js"></script>
    <script src="{{asset('assets')}}/js/contact-form.js"></script>
    <script src="{{asset('assets')}}/js/jquery.parallax-1.1.3.js"></script>
    <script src="{{asset('assets')}}/js/scrollUp.min.js"></script>
    <script src="{{asset('assets')}}/js/magnific-popup.min.js"></script>
    <script src="{{asset('assets')}}/js/wow.min.js"></script>
    <!--Main-active-JS-->
    <script src="{{asset('assets')}}/js/main.js"></script>
</body>

</html>
