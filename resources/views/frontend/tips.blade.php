@extends('templates.frontend')


@section('content')
<div class="container" >
  <div class="row">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h1>Tips</h1>
      </div>
      <div class="panel-body">

        <h3><b>Tips untuk test TOEIC</b></h3><br>
        <h4>1. Ubah settingan bahasa di gadget kamu ke Bahasa Inggris<h4><br>
          <p>Sangat sepele memang, tapi hal ini membuat kita tidak bergantung pada bahasa Indonesia saja.
            Mengoperasikan gadget yang full menggunakan bahasa Inggris akan melatih kalian untuk memahami
            kosakata yang jarang kita temui di pelajaran bahasa Inggris sekolah

            Jika sudah bisa mengoperasikan gadget dengan bahasa Inggris, lanjutkan dengan menggunakan bahasa Inggris di akun sosmed kalian.
            Ini juga akan membantu kalian dalam mengerjakan tes TOEIC reading section </p><br>

        <h4>2. Perbanyak baca buku Berbahasa Inggris<h4><br>
          <p>Buku adalah gudangnya vocabularies bahasa Inggris. Di buku, kalian bisa menemukan banyak sekali susunan kalimat yang bermacam-macam.
            Jenis buku berbahasa Inggris ada bermacam-macam, dari komik hingga novel bisa kalian buat sebagai sarana latihan membaca dan memahami bahasa Inggris.
            Hal ini akan membantu mengasah kemampuanmu dan menunjang saat tes TOEIC reading section.
         </p><br>

        <h4>3. Ngobrol menggunakan Bahasa Inggris<h4><br>
          <p>Salah satu cara untuk menguasai tes TOEIC adalah dengan membiasakan diri untuk ngobrol dengan menggunakan bahasa Inggris itu sendiri.
            Dengan ngobrol kamu akan mengasah kemampuan cara mengucapkan vocab dalam bahasa Inggris.<br><br>

            Hal ini akan sangat berdampak baik saat tes TOEIC nantinya karena akan membantu kamu memahami kosakata jika ada suara yang tidak
            terdengar jelas saat listening section. Kamu bisa menggunakan waktu luangmu untuk berlatih ngobrol bahasa Inggris ini dengan teman
            sekelas atau bahkan guru bahasa Inggrismu untuk mengasah kemampuanmu.
          </p><br>

          <h4>4. Tetap rajin dalam latihan soal TOEIC dan simulasikan<h4><br>
            <p>Sekarang di Internet sudah ada banyak sekali website yang menyediakan simulasi tes TOEIC salah satunya <i><b>Website ini.</b></i>
               Kalian dapat dengan mudah mengaksesnya di rumah dan melakukan simulasi cukup di depan layar komputer/laptop.<br><br>

               Soal latihan yang diberikan pun tidak terbatas sehingga kalian bisa melakukan simulasi sebanyak mungkin hingga kamu menguasai tes TOEIC ini,
               serta simulasi yang diberikan juga dibuat semirip mungkin dengan teknis aslinya sehingga memudahkan kamu untuk lebih siap menghadapi tes
               TOEIC yang sesungguhnya.</p><br>

      </div>
    </div>
  </div>
</div>
@endsection
