<?php

use Illuminate\Database\Seeder;
Use carbon\Carbon;
class PaketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $now = Carbon::now();
      DB::table('tbl_paket')->insert([
        ['paket'=>1, 'deskripsi_paket'=>'Easy','waktu_pengerjaan'=>3600, 'created_at'=>$now],
        ['paket'=>2, 'deskripsi_paket'=>'Hard','waktu_pengerjaan'=>3600, 'created_at'=>$now]
      ]);
    }

}
