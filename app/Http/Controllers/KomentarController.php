<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\Komentar;
use Auth;
use DataTables;
use Illuminate\Http\Request;

class KomentarController extends Controller
{

  public function komentar(){

    $komentar = DB::table('tbl_komentar')->orderBy('ket_waktu', 'DESC')->paginate(10);
    return view('backend/komentar',compact('komentar'));

  }

  public function destroy_komentar($id) {
             DB::delete('delete from tbl_komentar where id_komentar = ?',[$id]);
             $after_save = [
                       'alert' => 'success',
                       'icon' => 'check',
                       'title' => 'Berhasil ! ',
                       'text-1' => 'Komentar ',
                       'text-2' => 'Telah dihapus.'
                   ];
             return redirect('/komentar')->with('after_save', $after_save);

  }

}
