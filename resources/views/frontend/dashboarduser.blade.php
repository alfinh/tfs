@extends('templates/user')

@section('content')
<div class="container"><br><br><br><br>
  <div class="row" style="position:relative; top:20px;">
        <a href="/materireading">
          <div class="col-xs-12 col-sm-4">
              <div class="box">
                  <div class="box-icon">
                      <img src="{{asset('assets')}}/images/service-icon-2.png" alt="">
                  </div>
                  <h4>Reading</h4>
                  <p>berisikan materi-materi seputar reading yang didalamanya berisikan soal contohnya juga.</p>
              </div>
          </div>
          </a>
          <div class="col-xs-12 col-sm-4">
              <div class="box">
                <a href="/materilistening">
                  <div class="box-icon">
                      <img src="{{asset('assets')}}/images/service-icon-2.png" alt="">
                  </div>
                  <h4>Listening</h4>
                  <p>berisikan materi-materi seputar listening yang didalamanya berisikan soal contohnya juga.</p>
              </div>
          </div>
          <div class="col-xs-12 col-sm-4">
              <div class="box">
                <a href="/choosetype">
                  <div class="box-icon">
                      <img src="{{asset('assets')}}/images/portfolio-icon-6.png" alt="">
                  </div>
                  <h4>Questions Test</h4>
                  <p>Anda bisa mengetes kemampuan anda sebelum menghadapi test sesungguhnya agar lancar</i></b>.</p>
            </div>
        </div>
    </div>
</div>
@endsection
