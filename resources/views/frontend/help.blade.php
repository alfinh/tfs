@extends('templates.frontend')


@section('content')
<div class="container">
  <div class="row">
    <div class="panel panel-info">
        <div class="panel-heading">
          <h1>Cara Mengerjakan Test Soal</h1>
        </div>
          <div class="panel-body"><br>

            <ul class="nav nav-tabs sidebar-tabs" id="sidebar" role="tablist" style="margin-top:3px;">
               <li class="active"><a href="#tab-1" role="tab" data-toggle="tab">Step 1</a></li>
               <li><a href="#tab-2" role="tab" data-toggle="tab">Step 2</a></li>
               <li><a href="#tab-3" role="tab" data-toggle="tab">Step 3</a></li>
               <li><a href="#tab-4" role="tab" data-toggle="tab">Step 4</a></li>
            </ul><!--/.nav-tabs.sidebar-tabs -->
            <!-- Tab panes -->
            <div class="tab-content">
               <div class="tab-pane active" id="tab-1" style="color:grey; font-family:Calibri; font-size:17px;" >
                 <span style="font-size:22px; color:grey; font-family:Calibri;"><b>(Register / Login)</b></span><br>
                 <span  style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px;">
                  Untuk melaksanakan test soal, langkah pertama yang harus kita lakukan adalah dengan register atau login terlebih daluhu</div></span>
                   <br>

                  <img src="assets/images/help1.1.png" alt="" style="width:800px; height:500px;"><br><br>
                 <span style="font-size:22px; color:grey; font-family:Calibri;"><b></b></span><br>

                   • Gambar diatas adalah tampilan login / register, jika kalian sudah memiliki akun silahkan login dan apabila belum registrasi terlebih dahulu
               </div>

               <div class="tab-pane" id="tab-2" style="color:grey; font-family:Calibri; font-size:17px;" >
                   <span style="font-size:22px; color:grey; font-family:Calibri;"><b>Memilih Materi / Test Soal </b></span><br>
                   <br>
                   <span  style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px;">
                     Setelah login, kalian akan disajikan dengan 3 pilihan seperti gambar dibawah ini
                    </div>
                   </span>
                   <br><br>
                   <img src="assets/images/help2.png" alt="" style="width:1000px; height:500px;"><br><br>
                   <span  style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px;">
                   Disini kalian tinggal memilih "Questions Test" untuk melaksanakan ujian atau tes, jika kalian belum membaca materi sebaiknya kalian
                   membaca materi terlebih dahulu
                 </div>
                 </span>


                 </div><!--/.tab-pane -->

                 <div class="tab-pane" id="tab-3" style="color:grey; font-family:Calibri; font-size:17px;" >
                   <span style="font-size:22px; color:grey; font-family:Calibri;"><b>Memilih Paket</b></span><br>
                   <br>
                   <span  style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px;">
                       Jika meng-klik "Questions Test", kalian akan disajikan 2 pilihan paket</div></span>
                     <br>

                   <img src="assets/images/help3.png" alt="" style="width:1000px; height:500px;"><br><br>
                   <span  style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px;">
                     Kalian disarankan untuk mengerjakan paket 1 terlebih dahulu, dan jika kalian klik "Kerjakan Test"
                     Kalian akan diperlihatkan informasi dan ketentuan tentang paket 1</div></span><br>
                   <img src="assets/images/help3.1.png" alt="" style="width:800px; height:500px;"><br><br>
                   </div>

                 <div class="tab-pane" id="tab-4" style="color:grey; font-family:Calibri; font-size:17px;" >
                   <span style="font-size:22px; color:grey; font-family:Calibri;"><b>Mengerjakan Soal</b></span><br>
                   <br>
                   <span style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px;">
                     Ini adalah tahap terakhir dimana kalian harus mengerjakan soal paket 1, dan ini adalah tampilan Paket 1
                   </div></span>
                   <img src="assets/images/help4.png" alt="" style="width:1100px; height:500px;">
                   <br>

                   <b><i>• Selamat Mengerjakan •</b></i>
               </div><!--/.tab-pane -->
             </div><!--/.tab-content -->


          </div>
        </div>
      <div class="panel-body">
    </div>
  </div>
</div>
@endsection
