@extends('templates.frontend')


@section('content')
<div class="container" >
  <div class="row">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h1>Pemetaan Soal</h1>
      </div>
      <div class="panel-body">
        <h4> <b>Pertama-tama kita akan membahas soal <i>Reading</i></b> </h4><br>
        <h5>Soal Reading dibagi menjadi 3 :</h5>
        <p> 1. <b>Incomplete Sentence</b>
        <br>2. <b>Error Recognition</b>
        <br>3. <b>Comprehension</b></p><br>

        <p>• Kita akan membahas pada bagian <b>Incomplete Sentence </b><i>(40 Soal)</i><br><br>
          Pada bagian ini kemampuan yang dilatih adalah membaca dan penguasaan grammar serta vocabulary.
          Dibagian ini kalian akan membaca sebuah kalimat yang memiliki bagian kosong. kalau penguasaan grammer dan vocabulary kalian
          bagus, pasti soal ini akan terasa mudah dan juga kalian jangan sampai terkecoh, kalian harus teliti dalam mengerjakan soal apapun agar hasilnya maksimal.
        </p><br>

        <p>• Selanjutnya kita akan membahas pada bagian <b>Error Recognition </b><i>(20 Soal)</i><br><br>
          Pada bagian ini kalian dituntut untuk memilih salah satu kata yang salah atau yang tidak sesuai dengan struktur kalimat yang tersedia.
          Beberapa kata pada kalimat tersebut akan diberikan underline dan diberi huruf A, B, C dan D, Peserta harus memilih salah satu dari ke
          empat kata yang salah.Dibagian ini dibutuhkan ketelitian, dan kemampuan grammar yang baik.
        </p><br>

        <p>• Ini bagian terakhir dari <i>Soal Reading</i> yaitu <b>Comprehension</b><i>(40 Soal)</i><br><br>
          Pada bagian tes ini kalian akan menghadapi beberapa teks yang berupa surat, iklan, koran, artikel dll. Pada setiap teks mengandung 1-4
          pertanyaan yang harus dijawab oleh kalian. Kalian harus memahami dan mengerti maksud dan tujuan dari teks tersebut sebelum menjawab pertanyaan.
          Pola pertanyaannya yaitu :
          <br>- Pertanyaan tentang gagasan utama atau poin bahasan utama pada bacaan, kemungkinan judul yang tepat
          <br>- Pertanyaan tentang informasi yang secara langsung dinyatakan dalam teks
          <br>- Pertanyaan tentang informasi yang tersirat, disarankan atau disimpulkan
          <br>- Pertanyaan untuk mengenali aplikasi atau tujuan dari penulis
          <br>- Pertanyaan untuk mengevaluasi bagaimana penulis mengembangkan dan menyajikan bacaan
        </p><br>

        <h4><b>Selanjutnya kita akan membahas soal <i>Listening</i></b> </h4><br>
        <h5>Soal Listening dibagi menjadi 4 :</h5>
        <p> 1. <b>Photograph</b>
        <br>2. <b>Questions-Response</b>
        <br>3. <b>Short Conversation</b>
        <br>4. <b>Short Talk</b><br><br>

        <p>• Kita akan membahas pada bagian <b>Photograph</b><i>(20 Soal)</i><br><br>
          Pada bagian ini kalian akan mendengarkan 4 pernyataan tentang gambar yang ditampilkan dan pernyataan tersebut hanya akan diucapkan sekali.
          Dibagian ini kalian harus teliti dan harus fokus, karena pernyataan tidak akan diulang
        </p><br>

        <p>• Selanjutnya kita akan membahas pada bagian <b>Questions-Response</b><i>(30 Soal)</i><br><br>
          Pada bagian ini kalian akan mendengarkan 2 orang pembicara, pembicara pertama akan menanyakan sesuatu kemudian orang kedua akan merespon
          dengan 3 pilihan jawaban yang berbeda. Kalian harus memilih yang terbaik diantara 3 respon yang telah diberikan sesuai dengan pertayaan.
          Baik pertanyaan atau pernyataan dan juga respon hanya akan diperdengarkan satu kali. Semua  keterangan tersebut tidak akan ditampilkan, jadi kalian
          harus mendengarkan dengan seksama.
          Dibagian ini kalian harus teliti dan harus fokus, karena pernyataan tidak akan diulang
        </p><br>

        <p>• Disini kita akan membahas pada bagian <b>Short Conversation</b><i>(30 Soal)</i><br><br>
          Dibagian ini kalian akan mengerjakan 1-4 soal berdasarkan percakapan antara kedua orang. Percakapan tersebut tidak ditampilkan dan hanya diperdengarkan
          satu kali. Pada bagian ini memiliki 3 karakteristik yaitu :
          <br> 1. Pertanyaan seringkali sangat singkat.<br>
          <span style="padding-left:20px;">Contohnya <b><i>"Who is talking?"</i> dan <i>Where are they"</i></b></span>
          <br> 2. Pertanyaan seringkali menanyakan tentang informasi dan kata pertama diawali dengan jenis kata tanya.<br>
          <span style="padding-left:20px;">Contohnya <b><i>"When is the man's vacation?"</i> dan <i>"What does the client want?"</i></b></span>
          <br> 3. Pertanyaan seringkali dalam bentuk simple present tense atau present continuous tense.<br>
          <span style="padding-left:20px;">Contohnya <b><i>"Where are they going?"</i> dan <i>"Why is she changing her job?"</i></b><span>
        </p><br>

        <p>• Terakhir kita akan membahas tentang <b>Short Talk</b><i>(20 Soal)</i><br><br>
          Dibagian kalian akan mendengarkan beberapa kalimat yang diucapkan oleh seorang pembicara. Kalian akan diberikan pernyataan untuk menjawab
          1-3 pertanyaan tentang apa yang dibicarakan oleh pembicara dalam tiap pembicaraan. Yang ditampilkan hanya jawabannya saja sendangkan kalimat
          yang dibacakan oleh pembicara tidak ditampilkan. Disini kalian harus fokus dan tingkatkan konsentrasi kalian lagi serta memerlukan daya ingat yang tinggi,
          karena apa yang diucapkan oleh pembicara hanya 1 kali saja
        </p>


      </div>
    </div>
  </div>
</div>
@endsection
