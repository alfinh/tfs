@extends('templates/backend')

@section('judul')
Nilai
@endsection

@section('content')
<div class="main">
  <!-- MAIN CONTENT -->
  <div class="main-content">

    <div class="container-fluid">
      <!-- OVERVIEW -->
      <div class="panel panel-default">
      <div class="panel-heading">
        <span style="font-size:22px;">  Data Tester</span>
      </div>
      <br>
      {{-- part alert --}}
             @if (Session::has('after_save'))
                   <div class="alert alert-dismissible alert-{{ Session::get('after_save.alert') }}" style="margin:5px;">
                     <i class="pe-7s-{{ Session::get('after_save.icon') }}" style="font-size:30px; position:relative; top:8px;"></i>
                     <button type="button" class="close" data-dismiss="alert">×</button>
                     <strong>{{ Session::get('after_save.title') }}</strong>
                     <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_save.text-1') }}</a> {{ Session::get('after_save.text-2') }}
                   </div>
             @endif
       {{-- end part alert --}}
      <!-- /.panel-heading -->
    <div class="panel-body">
    <div class="table-responsive">
    <table  class="table table-striped table-bordered table-hover " id="tbl-tester" >
        <thead>
          <tr>
            <td><b>No</b></td>
            <td><b>Kode Test</b></td>
            <td><b>Id User</b></td>
            <td><b>Paket</b></td>
            <td><b>Nilai</b></td>
            <td><b>Waktu Mengerjakan</b></td>
            <td><b>ACTION</b></td>
            </tr>
        </thead>
      </table>
    </div>
  </div>
</div>
</div>
</div>
</div>
    @endsection

    @push('scripts')
    <script type="text/javascript">
    var t_tester;
    $(function() {
      t_nilai = $('#tbl-tester').DataTable({
            pagingType:"full_numbers",
            processing: true,
            "language": {
            processing: "<img src='{{asset('assets/images/2 (2).gif')}}'> "},
            serverSide: true,
            ajax:'{{ route("tester.json") }}',

            columns: [
                { data: null, orderable: false},
                { data: 'id_tester', name: 'id_tester' },
                { data: 'id_user', name:'id_user'},
                { data: 'id_paket', name:'id_paket'},
                { data: 'nilai', name: 'nilai'},
                { data: 'waktu_mengerjakan', name: 'waktu_mengerjakan' },
                { data: 'action', orderable:false, searchable:false }
              ],
              dom: 'B<"toolbar">ifrtlp',
              "rowCallback": function (nRow, aData, iDisplayIndex) {
               var oSettings = this.fnSettings ();
               $("td:first", nRow).html(oSettings._iDisplayStart+iDisplayIndex +1);
               return nRow;
             }
     } );

     // $("#btn-cari").on('click', function(e){
     //   t_nilai.ajax.reload();
     // });

     $(document).on('click', '.delete', function(){
           if(confirm("Are you sure you want to Delete this data?"))
           {
            alert('Record deleted successfully.'); window.location.href='/tester';
           }
           else
           {
               return false;
           }
       });
    });
    </script>
    @endpush
