@extends('templates/user')

@section('content')
<div class="container"><br><br><br><br>
    <div class="row">
       <div class="col-sm-12">
         <div class="panel">
           <div class="panel-heading" style="border:1px solid #eee"><h2>Materi Reading</h2></div>
            <div class="panel-body">
        <!--begin tabs going in narrow content -->
              <ul class="nav nav-tabs sidebar-tabs" id="sidebar" role="tablist" style="margin-top:3px;">
                 <li class="active"><a href="#tab-1" role="tab" data-toggle="tab">Incomplete</a></li>
                 <li><a href="#tab-2" role="tab" data-toggle="tab">Comprehension</a></li>
                 <li><a href="#tab-3" role="tab" data-toggle="tab">Recognition</a></li>
              </ul><!--/.nav-tabs.sidebar-tabs -->
              <!-- Tab panes -->
              <div class="tab-content">
                 <div class="tab-pane active" id="tab-1" style="color:grey; font-family:Calibri; font-size:17px;" >
                   <span style="font-size:22px; color:grey; font-family:Calibri;"><b>Materi Reading (Incomplete Sentece)</b></span><br>
                   <span  style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px;">

                       Pada bagian ini kemampuan yang diasah adalah membaca. Dibagian ini anda akan membaca sebuah kalimat yang memiliki bagian kosong.
                       Akan ada empat pilihan kata atau frasa yang bisa dipilih. Anda harus memilih satu yang menurut anda dapat melengkapi kalimat tersebut.
                       Ketika kalimatnya lengkap kalimat tersebut harus benar secara tata bahasanya.<br><br>
                       Pada bagian ini anda perlu pemahaman grammar dan penguasaan vocabulary</div></span><br>

                     <span style="font-size:22px; color:grey; font-family:Calibri;"><b>TIPS AND TRICK (Incomplete Sentece)</b></span><br>
                     <span  style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px;">
                       • Jangan khawatir tentang ejaan karena pilihan jawaban TOEIC tidak pernah berisi ejaan yang salah.<br>
                       • Baca dan pahami keseluruhan kalimat sebelum menjawab.<br>
                       • Waspadailah penggunaan preposition, adjective, conjunction, tense, dan bentuk kata yang salah.<br>
                       • Berhati-hatilah terhadap kata yang memiliki awalan atau akhiran yang sama. Misal : return, rebound.<br>
                       • Berhati-hatilah terhadap kata yang biasa salah digunakan. Misal : affect / effect<br></div></span>

                       <br><br>Contoh Incomplete Sentence<br><br>

                       Waiter : Welcome to Nusantara …?<br>
                       Anna   : Yes, Orange squash with little sugar please.<br><br>

                       A. Would you just pay it in cash?<br>
                       B. Do you need something to eat?<br>
                       C. How would you like your steak?<br>
                       D. Would you like something to drink?<br><br>
                       Pembahasan :<br><br>

                       Dari dialog diatas, kita bisa mencermati bahwa pelayan tersebut menawarkan minuman,
                       hal itu dilihat dari respon dari Anna yaitu “Orange squash with little sugar”.
                       Maka jawaban pertanyaan yang paling tepat adalah “Would you like something to drink?”<br><br>

                       Jawaban : D
                 </div><!--/.tab-pane -->

                 <div class="tab-pane" id="tab-2" style="color:grey; font-family:Calibri; font-size:17px;" >
                   <span style="font-size:22px; color:grey; font-family:Calibri;"><b>Materi Reading (Comprehension)</b></span><br><br>
                     <span  style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px; border-radius:2%;">
                       Comprehension Section bertujuan untuk menguji kemampuan Anda dalam memahami, menginterprestasikan,
                       dan menganalisa teks atau bacaan mengenai berbagai macam topik.</div></span><br>

                     <span  style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px; border-radius:2%;">
                       Setelah tahu bahwa tujuan membaca adalah menangkap ide dari sebuah teks, maka kita lanjutkan ke reading comprehension itu sendiri.
                       Reading comprehension merupakan pilar utama dalam aktivitas membaca dimana seorang pembaca membangun pemahaman terhadap sebuah teks.
                       Ia menggabungkan pemikiran logis yang dimiliki dengan kumpulan huruf, kata, dan kalimat yang ada di teks tersebut.</div></span><br>

                     <span  style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px; border-radius:2%;">
                       Jadi, reading comprehension di dalam teks bahasa Inggris bukan hanya mengenai bagaimana Anda membaca dengan baik yang mencakup ketepatan
                       pengucapan dan suara yang lantang. Namun, reading comprehension itu sendiri merupakan kegiatan membangun pemahaman makna sebuah teks
                       yang kemudian bisa terjemahkan oleh Anda dan melalui bahasa Anda sendiri. Lebih jauh pemahaman tersebut dapat bermanfaat bagi orang lain.
                       </div></span>
                     <br><br>Seperti Contoh Berikut<br>
                     <br>
                       A curfew is a specific type of law instituted by those In power. It is one that requires citizens to be off the streets and out of public places at specified hours.<br><br>
                       There are active curfew laws in some communities in the United States today; these laws are currently functioning.
                       The existing curfew laws generally refer to <i><u>minors</i></u>. These laws usually indicate the hour when the children must be off the streets and out of public unless they are with their parents.<br><br>

                       The word “minors” in paragraph 2 could be best replaced by …<br><br>

                       <br>(A) children
                       <br>(B) communities
                       <br>(C) public
                       <br>(D) citizens<br><br>

                       Jawab: (A) children<br>
                       Keyword: generally refer to minors<br>
                       Pembahasan:
                       <br>

                       <i>• Minors = belum dewasa</i><br>
                       <b>
                       • children = anak-anak<br>
                       • communities = komunitas<br>
                       • public = publik<br>
                       • citizens = warga</b></br>
                 </div>

                 <div class="tab-pane" id="tab-3" style="color:grey; font-family:Calibri; font-size:17px;" >
                   <span style="font-size:22px; color:grey; font-family:Calibri;"><b>Materi Reading (Error Recognition)</b></span><br>

                   <br>Error recognition terdiri dari:<br>

                   <span  style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px; border-radius:2%;">
                   <b>1.</b> Menentukan kata/frasa yang salah yang terdapat dalam kalimat yang menyatakan perbandingan benda/ peraturan atau penolakan.
                   Yang perlu dikuasai di sini utamanya Degrees of Comparison.</div></span><br>

                   <span  style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px; border-radius:2%;">
                   <b>2.</b> Menentukan kata/frasa yang salah yang terdapat dalam kalimat yang menyatakan deskripsi benda/ tempat wh-questions.
                   Yang perlu dikuasai di sini utamanya : adjective clause, relative pronoun, adjective in series dan WH questions.</div></span><br>

                   <span  style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px; border-radius:2%;">
                   <b>3.</b> Menentukan kata/frasa yang salah yang terdapat dalam kalimat yang menyatakan deskripsi fisik seseorang
                   (physical appearance)/benda atau profesi seseorang.Dalam hal ini, peserta harus menguasai: adjective clause, relative pronoun,
                   dan adjective in series (OSASCOM).</div></span><br>

                   <span  style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px; border-radius:2%;">
                   <b>4.</b> Menentukan kata/frasa yang salah yang terdapat dalam kalimat yang pengandaian/ tindakan.
                   Dalam hal ini peserta tes harus menguasai : Conditional Sentences (If Clause Type 1, 2, 3)</div></span><br>

                   <span  style="background:grey; color:black;"><div  style="background:#eee; color:grey; padding:5px; border-radius:2%;">
                   <b>5.</b> Menentukan kata/frasa yang salah yang terdapat dalam kalimat yang menyatakan perasaan seseorang terhadap s
                   esuatu/pendapat seseorang.Dalam hal ini peserta harus menguasai  : ungkapan sympathy, adj-ing dan adj-ed.</div></span><br>

                     <br><b>Ini Contoh Soal Error Recognition</b><br><br>
                     Manager  : <u>How</u>  would you like to work for this  project, with your previous team <u>or</u> a new team?<br>
                     <span style="margin-left:80px;">(A)</span>
                     <span style="margin-left:446px;">(B)</span><br>
                     Idhay        : I’d rather <u>to keep</u> my old team. We are a <u>solid</u>  team.<br>
                     <span style="margin-left:136px;">(C)</span>
                     <span style="margin-left:173px;">(D)</span><br>
                     Jawaban : C<br><br>
                     Pembahasan :<br>
                     Seharusnya “keep”
                </div><!--/.tab-pane -->
                </div><!--/.tab-content -->
            </div><br>
        </div>
    </div><!--/.row -->
</div><!--/.container -->
</div>
@endsection
