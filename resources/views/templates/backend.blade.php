<!doctype html>
<html lang="en">

<head>
	<title>TOEIC</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	@stack('css')
	<link rel="stylesheet" href="{{asset('assets')}}/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="{{asset('assets')}}/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{asset('assets')}}/vendor/linearicons/style.css">
	<link rel="stylesheet" href="{{asset('assets')}}/vendor/chartist/css/chartist-custom.css">
	<link rel="stylesheet" href="{{asset('assets')}}/css/jquery.dataTables.min.css">
	<link href="{{asset('assets')}}/css/pe-icon-7-stroke.css" rel="stylesheet" />
	<link href="{{asset('assets')}}/css/bootstrap-datepicker.min.css" rel="stylesheet" />
	<!-- MAIN CSS -->


	<link rel="stylesheet" href="{{asset('assets')}}/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="{{asset('assets')}}/css/demo2.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" href="{{asset('assets')}}/images/apple-touch-icon.png">
	<link rel="shortcut icon" type="{{asset('assets')}}/image/ico" href="{{asset('assets')}}/images/favicon.ico" />
</head>

<body>
	<!-- WRAPPER -->

	<div id="wrapper">
		<!-- NAVBAR -->

		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="brand">
				<a href="index.html"><img src="{{asset('assets')}}/images/loggin.png" alt="Klorofil Logo" class="img-responsive logo"></a>
			</div>
			<div class="container-fluid">
				<div class="navbar-btn">
					<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
				</div>
				<div class="navbar-form navbar-left">
					<span style="font-size:23px; color:grey; font-family:Calibri;">@yield('judul')</span>
				</div>


				<div id="navbar-menu">
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{asset('assets')}}/images/uf1.png" class="img-circle" alt="Avatar" style="width:25px;"> <span> Admin</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
							<ul class="dropdown-menu">
								<li><a href="#" data-toggle="modal" data-target="#ubahpassword"><i class="lnr lnr-lock"></i> <span>Ubah Password</span></a></li>
								<form action="{{ url('logout') }}" method="post">
									{{ csrf_field() }}
								<li style="padding-bottom:10px"><button type="submit" style="background:white; color:grey; border:0; margin-left:15px;"><i class="lnr lnr-exit"></i> <span>&nbsp Logout</span></button></li>
							</form>
							</ul>
						</li>
						<div class="pull-right">
						{{-- part alert --}}
								 @if (Session::has('after_savee'))
											 <div class="alert alert-dismissible alert-{{ Session::get('after_savee.alert') }}">
												 <i class="pe-7s-{{ Session::get('after_savee.icon') }}" style="font-size:30px; position:relative; top:8px;"></i>
												 <button type="button" class="close" data-dismiss="alert">×</button>
												 <strong>{{ Session::get('after_savee.title') }}</strong>
												 <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_savee.text-1') }}</a> {{ Session::get('after_savee.text-2') }}
											 </div>
									@endif
						 {{-- end part alert --}}
						</div>
					</ul>
				</div>
			</div>
		</nav>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<li><a href="/dashboard" class="active"><i class="lnr lnr-home"></i> <span style="font-family:Calibri; font-size:16px;">Dashboard</span></a></li>
						<li>
							<a href="#subPages" data-toggle="collapse" class="collapsed"><i class="lnr lnr-book"></i> <span style="font-family:Calibri; font-size:16px;">Soal</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="subPages" class="collapse ">
								<ul class="nav">
									<li><a href="/paketsatu" class="" style="font-family:Calibri; font-size:16px;"><i class="lnr lnr-file-empty"></i>Paket 1 (easy)</a></li>
									<li><a href="/paketdua" class="" style="font-family:Calibri; font-size:16px;"><i class="lnr lnr-file-empty"></i>Paket 2 (hard)</a></li>
								</ul>
							</div>
						</li>
						<li><a href="/user" class=""><i class="	lnr lnr-user"></i> <span style="font-family:Calibri; font-size:16px;">User Register</span></a></li>
						<li><a href="/tester" class=""><i class="	lnr lnr-bookmark"></i> <span style="font-family:Calibri; font-size:16px;">Data Test</span></a></li>
            <li><a href="/komentar" class=""><i class="lnr lnr-bubble"></i> <span>Komentar</span></a></li>
						</ul>
				</nav>
			</div>
		</div>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		@yield('content')
		<!-- END MAIN -->
		<div class="clearfix"></div>
    <footer class="footer">
              <div class="container-fluid">
                  <p class="copyright pull-right">
                      &copy; <script>document.write(new Date().getFullYear())</script> <a href="#">Alfi NH & Aziz </a>All Reserved
                  </p>
              </div>
          </footer>

		</div>
		<div class="modal fade" tabindex="-1" id="ubahpassword" role="dialog" data-backdrop="false">
			 <div class="modal-dialog" role="document">
				 <div class="modal-content">
			<!-- Modal Header -->
					 <div class="modal-header">
						 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							 <span aria-hidden="true"> &times;</span>
						 </button>
								 <h3 class="modal-title"><b>Ubah Password</b></h3>
					 </div>

							 <form action="/ubahpassword" method="POST" autocomplete="off">
								{{ csrf_field() }}
								<div class="modal-body">
									<div class="form-group" style="position: relative; top:10px;">
										<label for="name">Password Baru</label>
										<input type="password"  name="passwordbaru" class="form-control" placeholder="Masukkan Password Baru" required>
									</div>

									<div class="form-group" style="position: relative; top:10px;">
										<label for="name">Confirm Password</label>
										<input type="password"  name="passwordconfirm" class="form-control" placeholder="Ketik ulang password baru" required>
									</div>
							 </div>

					  	<div class="modal-footer">
								 <button type="reset" class="btn btn-raised btn-default "><i class="glyphicon glyphicon-refresh"> </i> Reset</button>
								 <button type="submit" class="btn btn-primary btn-save pull-right"><i class="glyphicon glyphicon-floppy-disk"></i> Submit</button>
  						</div>
					 </form>
			 </div>
 		</div>
 </div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="{{asset('assets')}}/vendor/jquery/jquery.min.js"></script>
	<script src="{{asset('assets')}}/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="{{asset('assets')}}/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="{{asset('assets')}}/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
	<script src="{{asset('assets')}}/js/klorofil-common.js"></script>
	<script type="text/javascript" src="{{asset('assets')}}/js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="{{asset('assets')}}/js/bootstrap-datepicker.js"></script>
	<script src="{{asset('assets')}}/js/jquery.dataTables.min.js" ></script>
	@stack('scripts')
</body>

</html>
