<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKomentar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tbl_komentar', function (Blueprint $table) {
           $table->increments('id_komentar');
           $table->string('nm_pengirim',100);
           $table->text('komentar');
           $table->timestamp('ket_waktu');
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('tbl_komentar');
     }
}
