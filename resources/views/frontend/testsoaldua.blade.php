<html>

<head>
    <meta charset="utf-8">
    <meta name="author" content="John Doe">
    <meta name="description" content="">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Title -->
    <title>TOEIC</title>
    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="{{asset('assets')}}/images/apple-touch-icon.png">
    <link rel="shortcut icon" type="{{asset('assets')}}/image/ico" href="{{asset('assets')}}/images/favicon.ico" />
    <!-- Plugin-CSS -->

    <link rel="stylesheet" href="{{asset('assets')}}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/themify-icons.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/magnific-popup.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/animate.css">
    <!-- Main-Stylesheets -->
    	<link rel="stylesheet" href="{{asset('assets')}}/css/main.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/normalize.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/style2.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/responsive.css">
    <script src="{{asset('assets')}}/js/vendor/modernizr-2.8.3.min.js"></script>
    <style>

    d: #fff
    }
    .panel-heading a {
        background: #f7f7f7;
        border-radius: 5px;
    }
    li{
       list-style-type: none;
    }
    .nav.nav-tabs li a,
    .nav.nav-tabs li.active > a:hover,
    .nav.nav-tabs li.active > a:active,
    .nav.nav-tabs li.active > a:focus {
        border-bottom-width: 0px;
        outline: none;
    }

    .tab-pane {
        background: #fff;
        padding: 10px;
        border: 1px solid #eee;
        margin-top: -1px;
    }



    /* used for sidebar tab/collapse*/
    @media (max-width: 991px) {
      .visible-tabs {
        display: none;
      }
    }

    @media (min-width: 992px) {
      .visible-tabs {
        display: block !important;
      }
    }

    @media (min-width: 992px) {
      .hidden-tabs {
        display: none !important;
      }
    }

    input[type="radio"]:checked { background: skyblue;}
    </style>
</head>

<body data-spy="scroll" data-target="#primary-menu" style="background-color:#F7F6F6;">
<br><br>
      <div class="container">
         <div class="row">
        <div class="panel">
          <div class="panel-body">
           <span style="font-family:Calibri; font-size:45px;"><u>Waktu Pengerjaan</u></span>
            <div><span id="time" style="font-family:Calibri; font-size:80px; margin-left:20px;"></span>&nbsp;<b style="margin-left:630px;">Status Pengerjaan : Belum Selesai</b></b></div>
           <div class="col-md-12">
                            <div class="alert alert-dismissible alert" style="background:skyblue; border:1px solid lightgrey; box-shadow:4px 7px #eee;">
                              <strong>Tester atas nama : {{Auth::user()->nama}} </strong>
                            </div>
            </div>
           <div class="col-sm-3 col-xs-5">
             <div class="panel"  style="border:1px solid lightgrey; box-shadow:4px 7px #eee;">
             <div class="panel-heading" style="background:skyblue;"><h2>No</h2></div>
             <div class="panel-body" style="display: flex;flex-direction: row;flex-basis: 66%;overflow-y: overlay; overflow-y:scroll; height:400px; padding:10px;">
              <table >
                <ul class="nav nav-tabs sidebar-tabs" id="sidebar" role="tablist" >
                  <?php
                 $a = 1;
                 $no = "";
                  echo '<tr>';
                   $i=0;
                   foreach ($data as $dataa){
                     if(strlen($a) == 1){
                       $no = "0".($a);
                     }else{
                       $no = ($a);
                     }

                     if($i%3==0 && $i!=0){
                      echo '</tr><tr><td style="padding:5px;"><a href="#'.$dataa->id_soal.'" data-toggle="tab" class="btn btn-md btn-fill btn-default" id="no_'.$dataa->id_soal.'">'.$no.'</a></td>';
                      }else{
                      echo '<td style="padding:5px;"><a href="#'.$dataa->id_soal.'" data-toggle="tab" class="btn btn-md btn-fill btn-default" id="no_'.$dataa->id_soal.'">'.$no.'</a></td>';
                      }
                     $i++; $a++;
                   }
                   echo '</tr>'; ?>
              </ul>
              </table>
            </div>
           </div>
         </div>
            <div class="col-sm-9 col-xs-7">
            <form action="{{url('/validasijawabandua')}}"id="testing" method="post">
            {{ csrf_field() }}
            <div class="panel" style="border:1px solid lightgrey; box-shadow:4px 7px #eee;">
              <div class="panel-heading"style="background:skyblue;"><h2>Soal</h2></div>
            <div class="tab-content" id="tabs">
              <?php
              $i = 1;
              $c = 1;
              ?>
            @foreach($data as $dataa)
            <div class="tab-pane tab_{{$i}} @php echo ($i==1) ? 'active' : '' @endphp" id="{{$dataa->id_soal}}">
              <p><b>Type : {{$dataa->tipe_soal}}</b></p>
              <p>{{$dataa->keterangan}}</p>
              <p><b><?php echo $c++;?>.</b> {{$dataa->soal_tulisan}}</p>
              <div class="table-responsive ">
                <hr style="border:1px solid lightgrey;">
              <center>@if($dataa->soal_gambar != '-.png')<img src="{{asset('uploads')}}/file/{{$dataa->soal_gambar}}" style="max-width:1000px; max-height:300px;">@else  @endif</center>
                <hr style="border:1px solid lightgrey;">
              @if($dataa->soal_suara == '-')
              &nbsp;
              @else
              <audio src="{{asset('uploads')}}/file/{{$dataa->soal_suara}}" id="suara"controls>
              </audio>
              @endif
              </div>
              <p><input type="radio" onclick="changeColorSoal({{$dataa->id_soal}})" name="jawaban[{{$dataa->id_soal}}]" id="jawaban" value="a">{{$dataa->jawaban_a}}</i></p>
              <p><input type="radio" onclick="changeColorSoal({{$dataa->id_soal}})" name="jawaban[{{$dataa->id_soal}}]" id="jawaban" value="b">{{$dataa->jawaban_b}}</i></p>
              <p><input type="radio" onclick="changeColorSoal({{$dataa->id_soal}})" name="jawaban[{{$dataa->id_soal}}]" id="jawaban" value="c">{{$dataa->jawaban_c}}</i></p>
              <p><input type="radio" onclick="changeColorSoal({{$dataa->id_soal}})" name="jawaban[{{$dataa->id_soal}}]" id="jawaban" value="d">{{$dataa->jawaban_d}}</i></p>
            </div>
            <?php $i++; ?>
            @endforeach
            <div class="panel-footer " style="background:skyblue;"><div class="col-sm-offset-8"><button type="button" onclick="nextSoal('-1')" id="btnKembali" class="btn btn-md btn-default btn-fill">Kembali</button>
            <button type="button" onclick="nextSoal(1)" class="btn btn-md btn-default btn-fill" id="btnSelanjutnya">Selanjutnya</button>
            <button type="submit" style="display: none;" class="btn btn-md btn-default btn-fill" id="btnSubmit">Submit</button>
          </div></div>
            </div>
            <br>
          </div>
        </form>
         </div>
      </div>
    </div>
  </div>
</div>`

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>

<script>

//jquery about button submit-kembali-selanjutnya -----------------------------------------------

var soal_tab = 1;
var max_tab = {{$i-1}};

function activaTab(tab){
    $('.nav-tabs a[href="#' + tab + '"]').tab('show');
};

function checkTab(){
  if(soal_tab == 1){
    $('#btnKembali').hide();
    $('#btnSelanjutnya').show();
  }else{
    $('#btnKembali').show();
    $('#btnSelanjutnya').show();
  }

  if(soal_tab == max_tab){
    $('#btnSubmit').show();
    $('#btnSelanjutnya').hide();
  }else{
    $('#btnSubmit').hide();
  }
}

//batas------------------------------------------------------------------------------------------

//React NoSoal-----------------------------------------------------------------------------------
function nextSoal(i){
  if(i == 1){
    soal_tab++;
  }else if(i == "-1"){
    soal_tab = soal_tab - 1;
  }

  $('.tab-pane').removeClass('active');
  $('.tab_'+soal_tab).addClass('active');
  checkTab();
}


function changeColorSoal(id_soal){
  console.log(id_soal);
  $('#no_'+id_soal).addClass('btn-info');
  $('#no_'+id_soal).removeClass('btn-default');
}

//batas------------------------------------------------------------------------------------------

//Countdown Timer test---------------------------------------------------------------------------

function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    minutes = parseInt(timer / 60, 10);
    seconds = parseInt(timer % 60, 10);


    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;


    display.text(minutes + ":" + seconds);

    if (--timer < 0) {
        timer = duration;
    }

    timeSpend = timer
    setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);


        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;


        display.text(minutes + ":" + seconds);

        if (--timer < 0) {
            timer = duration;
        }

        timeSpend = timer;

    }, 1000);
}

//batas------------------------------------------------------------------------------------------

//Get data test before unload & Serialize--------------------------------------------------------

window.onbeforeunload = function() {

    localStorage.setItem("jawaban", $('#jawaban').val());
    localStorage.setItem("soal_jawaban", $('#testing').serialize());
    localStorage.setItem("time", timeSpend);

    jawabanSer = $('#testing').serialize();
    timeSer = $('#time').val();

    $.ajax({
      type: 'GET',
      async: false,
      url: "{{url('/updatewaktudua')}}",
      data: "waktu_mengerjakan="+timeSpend+"&jawaban="+jawabanSer
    });

}

//batas------------------------------------------------------------------------------------------

//When Data test to unserialize -----------------------------------------------------------------

function unserialize(serializedString){
  var str = decodeURI(serializedString);
  var pairs = str.split('&');
  var obj = {}, p, idx, val;
  for (var i=0, n=pairs.length; i < n; i++) {
    p = pairs[i].split('=');
    idx = p[0];

    if (idx.indexOf("[]") == (idx.length - 2)) {
      // Eh um vetor
      var ind = idx.substring(0, idx.length-2)
      if (obj[ind] === undefined) {
        obj[ind] = [];
      }
      obj[ind].push(p[1]);
    }
    else {
      obj[idx] = p[1];
    }
  }
  return obj;
};

//batas------------------------------------------------------------------------------------------

//When data roll & onload to page back ----------------------------------------------------------

window.onload = function() {


    var timeSpend = <?= $data_tes->waktu_mengerjakan  ?>;
    display = $('#time');

    if(timeSpend == "NaN" || timeSpend == "0"){
      timeSpend = <?php echo $waktu ; ?>;
      startTimer(timeSpend, display);
    }else{
      startTimer(timeSpend, display);
    }

    checkTab();

    var jawaban_soal = JSON.parse('<?= $data_tes->jawaban ?>');

    $.each(jawaban_soal, function(key, val){
      $('input[name="jawaban['+key+']"][value="'+val+'"]').prop('checked',true);
      console.log(key);

          $("#no_"+key).addClass('btn-info');
          $("#no_"+key).removeClass('btn-default');

    });

}

//batas------------------------------------------------------------------------------------------

//Tab collapse for nosoal when click ------------------------------------------------------------

$(document).ready(function() {

    // if the tabs are in a narrow column in a larger viewport
    $('.sidebar-tabs').tabCollapse({
        tabsClass: 'visible-tabs',
        accordionClass: 'hidden-tabs'
    });


    // initialize tab function
    $('.nav-tabs a').click(function(e) {
        e.preventDefault();
        $(this).tab('show');
    });

    // slide to top of panel-group accordion
    $('.panel-group').on('shown.bs.collapse', function() {
        var panel = $(this).find('.in');
        $('html, body').animate({
            scrollTop: panel.offset().top + (-60)
        }, 500);
    });

});

//batas------------------------------------------------------------------------------------------

</script>
<!--General-JS-->
<script src="{{asset('assets')}}/js/vendor/jquery-1.12.4.min.js"></script>
<script src="{{asset('assets')}}/js/vendor/bootstrap.min.js"></script>
<!--Plugin-JS-->
<script src="{{asset('assets')}}/js/owl.carousel.min.js"></script>
<script src="{{asset('assets')}}/js/contact-form.js"></script>
<script src="{{asset('assets')}}/js/jquery.parallax-1.1.3.js"></script>
<script src="{{asset('assets')}}/js/scrollUp.min.js"></script>
<script src="{{asset('assets')}}/js/magnific-popup.min.js"></script>
<script src="{{asset('assets')}}/js/wow.min.js"></script>
<!--Main-active-JS-->
<script src="{{asset('assets')}}/js/main.js"></script>
</body>
</html>
