<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//frontend-------------------------------------------------------------------------------

Route::get('/', 'FrontendController@index');
Route::get('/pengertian', 'FrontendController@pengertian');
Route::get('/tips', 'FrontendController@tips');
Route::get('/syarat', 'FrontendController@syarat');
Route::get('/caradaftar', 'FrontendController@caradaftar');
Route::get('/perbedaan', 'FrontendController@perbedaan');
Route::get('/pemetaansoal', 'FrontendController@pemetaansoal');
Route::get('/help', 'FrontendController@help');
Route::get('/registerform', function(){
  return view('frontend\register');
});

Route::auth();

Route::post('/registeruser','FrontendController@registeruser');

Route::group(['middleware'=>['auth','IsUser']], function(){

Route::get('/choosetype','FrontendController@type');
Route::any('/ubahpassworduser','PengaturanController@ubahpassword');
Route::get('/dashboarduser','FrontendController@dashboarduser');
Route::get('/materireading', 'FrontendController@materireading');
Route::get('/materilistening', 'FrontendController@materilistening');
Route::get('/profile', function(){
  return view('frontend\profile');
});
Route::post('/editakun','PengaturanController@editakun');

//paket1
Route::any('/checkedtest','FrontendController@check');
Route::any('/testsoalsatu', 'FrontendController@testsoalsatu');
Route::post('/validasijawabansatu','TestController@storetestsatu');
Route::get('/nilai','TestController@nilai')->name('nilai');
Route::get('/updatewaktu','FrontendController@updatewaktu');
//paket2
Route::any('/checkedtest2','FrontendController@check2');
Route::any('/testsoaldua', 'FrontendController@testsoaldua');
Route::post('/validasijawabandua','TestController@storetestdua');
Route::get('/nilaidua','TestController@nilaidua')->name('nilaidua');
Route::get('/updatewaktudua','FrontendController@updatewaktudua');

Route::post('/validasikomentar','TestController@komentar');
Route::get('/history','TestController@history');
Route::get('/sementara', function(){
  return view('sementara');
});
});
//Batas----------------------------------------------------------------------------------


//Backend-------------------------------------------------------------------------------
Route::group(['middleware'=>['auth','IsAdmin']], function(){
//Pengaturan
Route::any('/ubahpassword','PengaturanController@ubahpassword');

Route::get('/dashboard', 'BackendController@dashboard');

//SoalPaket 1 & 2
Route::any('/paketsatu', 'SoalController@paketsatu')->name('paketsatu');
Route::any('/paketsatu/json','SoalController@jsonsatu')->name('paketsatu.json');
Route::any('/paketdua', 'SoalController@paketdua')->name('paketdua');
Route::any('/paketdua/json','SoalController@jsondua')->name('paketdua.json');
Route::get('delete_soalsatu/{id}','SoalController@destroy_soalsatu');
Route::get('delete_soaldua/{id}','SoalController@destroy_soaldua');
Route::get('/create_soal','SoalController@create_satu');
Route::post('/validasisoal','SoalController@store_soal');
Route::get('/{id}/showmenusoal','SoalController@show_soal');
Route::any('/updatesoal','SoalController@update_soal');

//user
Route::any('/user','UserController@user')->name('user');
Route::any('/user/json','UserController@json')->name('user.json');
Route::get('delete_user/{id}','UserController@destroy');


//tester
Route::any('/tester','TesterController@tester')->name('tester');
Route::any('/tester/json','TesterController@json')->name('tester.json');
Route::get('delete_tester/{id}','TesterController@destroy');

//Komentar
Route::get('/komentar','KomentarController@komentar');
Route::get('delete_komentar/{id}','KomentarController@destroy_komentar');

//Batas----------------------------------------------------------------------------------

});
