@extends('templates/backend')

@section('judul')
Dashboard
@endsection


@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

<link rel="stylesheet" href="{{asset('assets')}}/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{asset('assets')}}/dist/css/skins/_all-skins.min.css">
<!-- iCheck -->
<link rel="stylesheet" href="{{asset('assets')}}/plugins/iCheck/flat/blue.css">
<!-- Morris chart -->
<link rel="stylesheet" href="{{asset('assets')}}/plugins/morris/morris.css">
<!-- jvectormap -->
<link rel="stylesheet" href="{{asset('assets')}}/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Date Picker -->
<link rel="stylesheet" href="{{asset('assets')}}/plugins/datepicker/datepicker3.css">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{asset('assets')}}/plugins/daterangepicker/daterangepicker.css">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{asset('assets')}}/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
@endpush

@section('content')

<div class="main">
  <!-- MAIN CONTENT -->
  <div class="main-content">
    <div class="container-fluid">
      <!-- OVERVIEW -->
      <div class="panel panel-headline">
        <div class="panel-heading">
          <h3 class="panel-title">Monthly Overview</h3>
          <p class="panel-subtitle">Periode: {{$bulansebelumnya}} 2018 - {{$bulanini}} 2018</p>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                      <div class="inner">
                        <h3>{{$soalreading}}</h3>

                        <p>Soal Reading</p>
                      </div>
                      <div class="icon">
                        <i class="ion ion-bookmark"></i>
                      </div>
                      <a href="{{URL('/data_transaksi')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                  </div>
                  <!-- ./col -->
                  <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                      <div class="inner">
                        <h3>{{$soallistening}}</h3>

                        <p>Soal Listening</p>
                      </div>
                      <div class="icon">
                        <i class="ion ion-ios-book"></i>
                      </div>
                      <a href="{{URL('/data_transaksi')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                  </div>
                  <!-- ./col -->
                  <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                      <div class="inner">
                        <h3>{{$usertester}}</h3>

                        <p>Test dikerjakan</p>
                      </div>
                      <div class="icon">
                        <i class="ion ion-person-add"></i>
                      </div>
                      <a href="{{URL('/tester')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                  </div>
                  <!-- ./col -->
                  <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                      <div class="inner">
                        <h3>{{$userregister}}</h3>

                        <p>User Register</p>
                      </div>
                      <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                      </div>
                      <a href="{{URL('/user')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                  </div>
            </div>
            <div class="row" >
                  <div class="col-md-6">
                      <div class="card">
                         <div class="panel panel-default" >
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <canvas id="perhari" width="800" height="500"></canvas>
                            </div>
                          </div>
                      </div>
                  </div>
                    <div class="col-md-6">
                      <div class="card">
                        <div class="panel panel-default" >
                                <!-- /.panel-heading -->
                           <div class="panel-body">
                             <canvas id="perbulan" width="800" height="500"></canvas>
                           </div>
                        </div>
                     </div>
                   </div>
             </div>
</div>
          <!-- END REALTIME CHART -->
        </div>
      </div>
    </div>
  </div>
  <!-- END MAIN CONTENT -->

</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{asset('assets')}}/js/chart.min.js"></script>
<script type="text/javascript">


//laporan perhari
new Chart(document.getElementById("perhari"), {
    type: 'bar',
    data: {
      labels: ["Tanggal 1", "Tanggal 2", "Tanggal 3", "Tanggal 4", "Tanggal 5","Tanggal 6",
               "Tanggal 7","Tanggal 8","Tanggal 9","Tanggal 10","Tanggal 11","Tanggal 12","Tanggal 13",
               "Tanggal 14","Tanggal 15","Tanggal 16","Tanggal 17","Tanggal 18","Tanggal 19","Tanggal 20",
               "Tanggal 21","Tanggal 22","Tanggal 23","Tanggal 24","Tanggal 25","Tanggal 26","Tanggal 27",
               "Tanggal 28","Tanggal 29","Tanggal 30","Tanggal 31"],
      datasets: [
        {
          label: "Jumlah Tester",
          backgroundColor: ["#3e95cd","#8e5ea2","#3cba9f","#e8c3b9","#c45850","#3e95cd","#8e5ea2",
                            "#3cba9f","#e8c3b9","#c45850","#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9",
                            "#c45850","#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850","#3e95cd",
                            "#8e5ea2","#3cba9f","#e8c3b9","#c45850","#3e95cd", "#8e5ea2","#3cba9f",
                            "#e8c3b9","#c45850","#3e95cd"],
          // data: [tanggal1,tanggal2,tanggal3,tanggal4,tanggal5,tanggal6,tanggal7,tanggal8,tanggal9,tanggal10,tanggal11,
          //        tanggal12,tanggal13,tanggal14,tanggal15,tanggal16,tanggal17,tanggal18,tanggal19,tanggal20,tanggal21,tanggal22,tanggal23,
          //        tanggal24,tanggal25,tanggal26,tanggal27,tanggal28,tanggal29,tanggal30,tanggal31]

          data :[12,13,15,18,19,11,8,9,14,19,24,19,11,8,9,14,19,24,19,11,8,9,14,19,24,11,8,9,14,19,24]
        }
      ]
    },
    options: {
         scales: {
             yAxes: [{
                 ticks: {
                     beginAtZero:true
                 }
             }]
         }
     }
});
//tamat laporan perhari



//Laporan perbulan
new Chart(document.getElementById("perbulan"), {
  type: 'doughnut',
  data: {
    labels: ["Januari", "Febuari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus",
             "September", "Oktober", "November", "Desember"],
    datasets: [
      {
        label: "Jumlah Tester",
        backgroundColor: ["#3e95cd","#8e5ea2","#3cba9f","#e8c3b9","#c45850","#3e95cd","#8e5ea2",
                          "#3cba9f","#e8c3b9","#c45850","#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9",
                          "#c45850","#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850","#3e95cd",
                          "#8e5ea2","#3cba9f","#e8c3b9","#c45850","#3e95cd", "#8e5ea2","#3cba9f",
                          "#e8c3b9","#c45850","#3e95cd"],
        data: [10,15,10,15,10,10,14,12,31,13,14,32]
      }
    ]
  },
  options: {
    legend: { display: false },
    title: {
      display: true,
    text: 'Laporan Per Bulan'
  }
}
});
//Tamat laporan perbulan




</script>
@endpush
