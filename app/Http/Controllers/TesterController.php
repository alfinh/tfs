<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\Nilai;
use App\Tester;
use Auth;
use DataTables;
use Illuminate\Http\Request;

class TesterController extends Controller
{
  public function tester()
   {

    return view('backend/tester');
   }


  public function json(){

        $tester = Tester::All();
        return Datatables::of($tester)
        ->addColumn('action', function($tester){
        return '<a href="delete_tester/'.$tester->id_tester.'" class="btn btn-sm btn-danger delete" id=""><i class="glyphicon glyphicon-remove"></i></a>';

        })

        ->make(true);
  }

  public function destroy($id) {
             DB::delete('delete from tbl_tester where id_tester = ?',[$id]);
             $after_save = [
                       'alert' => 'success',
                       'icon' => 'check',
                       'title' => 'Berhasil ! ',
                       'text-1' => 'Data Tester ',
                       'text-2' => 'Telah dihapus.'
                   ];
             return redirect('/tester')->with('after_save', $after_save);

  }
}
