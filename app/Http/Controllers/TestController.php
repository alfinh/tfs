<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Nilai;
use App\Tester;
use Carbon\Carbon;
use Auth;
class TestController extends Controller
{
    //
    public function storetestsatu(Request $request)
    {

      $getSoal = DB::table('tbl_soal')->get();
      $jawaban = $request->input('jawaban');
      $jumlah_benar = 0;
      foreach($getSoal as $dataSoal){
        $fromSoal = $jawaban[$dataSoal->id_soal];
        $jawabanAsli = $dataSoal->jawaban_asli;

        if($fromSoal == $jawabanAsli){
          $jumlah_benar++;
        }
      }

      $jumlah_salah = count($getSoal) - $jumlah_benar;
      $now = Carbon::now();
      $nilai = round((100/count($getSoal))*$jumlah_benar, 0);
      $yearnow = Carbon::now()->format('Y');
      $monthnow = Carbon::now()->format('m');
      $daynow = Carbon::now()->format('d');
      $idrandom= rand(10,99);
      $waktu = '60 Menit';
      $id_paket =1;
      $auth=Auth::user()->id;
      $idakhir = "$yearnow$monthnow$daynow$idrandom";

      $data = DB::table('tbl_tester')->where([
        ['id_user','=',$auth],
        ['id_paket','=',1]
        ]
      )->update([
        'nilai'=>$nilai,
      ]);



      return redirect('/nilai');
    }

    public function nilai(){
      $id = Auth::user()->id;
      $data = Tester::where('id_user','=',$id)->where('id_paket','=',1)->first();

      return view('frontend/nilaisatu',compact('data'));
    }



    public function storetestdua(Request $request)
    {

      $getSoal = DB::table('tbl_soal')->where('id_paket',2)->get();
      $jawaban = $request->input('jawaban');
      $jumlah_benar = 0;
      foreach($getSoal as $dataSoal){
        $fromSoal = $jawaban[$dataSoal->id_soal];
        $jawabanAsli = $dataSoal->jawaban_asli;

        if($fromSoal == $jawabanAsli){
          $jumlah_benar++;
        }
      }

      $jumlah_salah = count($getSoal) - $jumlah_benar;
      $now = Carbon::now();
      $nilai = round((100/count($getSoal))*$jumlah_benar, 0);
      $yearnow = Carbon::now()->format('Y');
      $monthnow = Carbon::now()->format('m');
      $daynow = Carbon::now()->format('d');
      $idrandom= rand(10,99);
      $waktu = '60 Menit';
      $id_paket =2;
      $auth=Auth::user()->id;
      $idakhir = "$yearnow$monthnow$daynow$idrandom";

      $data = DB::table('tbl_tester')->where([
        ['id_user','=',$auth],
        ['id_paket','=',2]
        ]
      )->update([
        'nilai'=>$nilai,
      ]);



      return redirect('/nilaidua');
    }


    public function nilaidua(){
      $id = Auth::user()->id;
      $data = Tester::where('id_user','=',$id)->where('id_paket','=',2)->first();

      return view('frontend/nilaidua',compact('data'));
    }


    public function komentar(Request $request){
      $waktu = Carbon::now();
      $komentar = $request->input('komentar');
      $after_save = [
                'alert' => 'success',
                'icon' => 'check',
                'title' => 'Berhasil ! ',
                'text-1' => 'Anda telah  ',
                'text-2' => 'Mengerjakan Test.'
            ];

      DB::table('tbl_komentar')->insert([
        ['nm_pengirim'=>Auth::user()->nama, 'komentar'=>$komentar,  'ket_waktu'=>$waktu]
      ]);

      return redirect('/choosetype')->with('after_save', $after_save);

    }

    public function history(){

      $auth = Auth::user()->id;
      $history = DB::select("SELECT * FROM tbl_tester where id_user=$auth");

      return view('frontend/history',compact('history'));

    }
}
