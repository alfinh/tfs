<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $now = Carbon::now();
      DB::table('users')->insert([
        ['nama'=>'Admin', 'username'=>'admin', 'password'=>bcrypt('admin12345'), 'isadmin'=>1, 'created_at'=>$now],
        ['nama'=>'Alfi', 'username'=>'alfi', 'password'=>bcrypt('user12345'), 'isadmin'=>0, 'created_at'=>$now]
      ]);
    }
}
